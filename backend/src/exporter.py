from src.models import MesureBruit, LocalisationMesure, ResultatMesureBruit, SourceDeBruit, \
    RelationSourceMesure, Route, InfraRoute, TraficRoutier
from src.pydantic_models import MandatoryFormInput
from src.importer import nextval_id_localisation_mesure, nextval_id_mesure_bruit, nextval_id_resultat_bruit, \
    nextval_id_route, nextval_id_infra_route, nextval_id_trafic_routier, nextval_id_source_de_bruit, nextval_id_source_mesure


class Exporter:
    def __init__(self, session_instance):
        self.session_instance = session_instance

    def export_to_databases(self, body: MandatoryFormInput):
        

        # table localisation_mesure
        if body.id_localisation_mesure:
            id_loc = body.id_localisation_mesure
        else:
            id_loc = nextval_id_localisation_mesure(self.session_instance)
        localisation_mesure = LocalisationMesure(
            id_localisation_mesure=id_loc,
            geom=self.convert_coordinates(body.x_coordinate_epsg2154, 
                                         body.y_coordinate_epsg2154),
            commentaire=body.comment_localisation
        )
        self.base_export_to_databases(value=localisation_mesure)

        # Table mesure de bruit
        id_mesure_bruit = nextval_id_mesure_bruit(self.session_instance)
        mesure_bruit = MesureBruit(
            id_mesure_bruit=id_mesure_bruit,
            maitre_ouvrage=body.maitre_ouvrage,
            maitre_oeuvre=body.maitre_oeuvre,
            is_point_fixe=body.is_point_fixe,
            id_point_fixe=body.id_point_fixe,
            id_localisation_mesure=id_loc,
            is_exterieur=body.is_exterieur,
            is_facade=body.is_facade,
            is_champ=body.is_champ,
            hauteur_mesure=body.hauteur_mesure,
            id_piece_mesure=body.id_piece_mesure,
            id_piece_mesure_orient=body.id_piece_mesure_orient,
            is_ponctuel=body.is_ponctuel,
            date_debut=body.date_debut,
            date_fin=body.date_fin,
            duree_integration=body.duree_integration,
            id_materiel_mesure=body.id_materiel_mesure,
            id_rapport=body.id_rapport,
            id_campagne_mesure=body.id_campagne_mesure,
            id_meteo=body.id_meteo,
            commentaire=body.comment_mesure
        )
        self.base_export_to_databases(value=mesure_bruit)

        # Table resultat
        # iteration sur chaque dico présent dans la clé "resultats"
        for r in body.resultats:
            id_resultat_bruit = nextval_id_resultat_bruit(self.session_instance)
            resultat_mesure = ResultatMesureBruit(
                id_resultat_mesure_bruit=id_resultat_bruit,
                id_mesure_bruit=id_mesure_bruit,
                id_nature_mesure=r.id_nature_mesure,
                id_indicateur=r.id_indicateur,
                id_unite_grandeur=r.id_unite_grandeur,
                id_ponderation=r.id_ponderation,
                debut_periode=r.debut_periode,
                fin_periode=r.fin_periode,
                is_periode_reference=r.is_periode_reference,
                valeur=r.valeur,
                incertitude=r.incertitude,
                commentaire=r.comment_resultat
            )
            self.base_export_to_databases(value=resultat_mesure)


        # Table source_de_bruit
        id_source_de_bruit = nextval_id_source_de_bruit(self.session_instance)
        source_de_bruit = SourceDeBruit(
            id_source_de_bruit=id_source_de_bruit,
            type_source_bruit=body.type_source_bruit,
            commentaire=body.comment_source_de_bruit
        )
        self.base_export_to_databases(value=source_de_bruit)

        # Table relation_source_mesure
        id_source_mesure = nextval_id_source_mesure(self.session_instance)
        relation_source_mesure = RelationSourceMesure(
            id_source_mesure=id_source_mesure,
            id_source_de_bruit=id_source_de_bruit,
            id_mesure_bruit=id_mesure_bruit
        )
        self.base_export_to_databases(value=relation_source_mesure)

        # Table route
        # On ne devrait pas créer un id_route si existe déjà (comment faire?)
        id_route = nextval_id_route(self.session_instance)
        route = Route(
            id_route=id_route,
            id_nom_route=body.id_nom_route,
            id_gest_route=body.id_gest_route,
            id_dept=body.id_dept,
            id_commune=body.id_commune
        )
        self.base_export_to_databases(value=route)

        # Table infra_route
        id_infra_route = nextval_id_infra_route(self.session_instance)
        infra_route = InfraRoute(
            id_infra_route=id_infra_route,
            id_route=id_route,
            largeur=body.largeur_route,
            nb_voie=body.nb_voie,
            declivite=body.declivite,
            sens_circulation=body.sens_circulation,
            id_rvt_nature=body.id_rvt_nature,
            id_rvt_etat=body.id_rvt_etat,
            id_rvt_granulo=body.id_rvt_granulo,
            is_rvt_sec=body.is_rvt_sec
        )
        self.base_export_to_databases(value=infra_route)

        # Table trafic_routier
        for r in body.trafics_routiers:
            id_trafic_routier = nextval_id_trafic_routier(self.session_instance)
            trafic_routier = TraficRoutier(
                id_trafic_routier=id_trafic_routier,
                id_infra_route=id_infra_route,
                id_source_de_bruit=id_source_de_bruit,
                date_heure_debut=r.date_heure_debut,
                date_heure_fin=r.date_heure_fin,
                indicateur=r.indicateur,
                valeur=r.valeur,
                trafic_nature=r.trafic_nature
            )
            self.base_export_to_databases(value=trafic_routier)

        return body


    def base_export_to_databases(self, value):
        with self.session_instance() as session:
            session.add(value)
            session.commit()

    def convert_coordinates(self, x, y):
        return f'SRID=2154;POINT({x} {y})'
        
