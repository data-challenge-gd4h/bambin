from sqlalchemy.orm import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, Boolean, Sequence
from geoalchemy2 import Geometry

Base = declarative_base()


class User(Base):
    __tablename__ = "user"
    __table_args__ = {"schema": "si"}
    id_user = Column(Integer, primary_key=True)
    firstname = Column(String, nullable=False)
    lastname = Column(String, nullable=False)
    mail = Column(String, nullable=False)
    numero_siren = Column(String, nullable=False)
    type_source_bruit = Column(Integer)
    is_authorized = Column(Boolean, nullable=False)

    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (
            self.firstname,
            self.lastname,
            self.mail,
        )


class MesureBruit(Base):
    __tablename__ = "mesure_bruit"
    __table_args__ = {"schema": "mesure_bruit"}
    id_mesure_bruit = Column(
        Integer,
        Sequence("mesure_bruit_id_mesure_bruit_seq", schema="mesure_bruit"),
        primary_key=True,
    )
    maitre_ouvrage = Column(String, nullable=False)
    maitre_oeuvre = Column(String)
    is_point_fixe = Column(Boolean, nullable=False)
    id_point_fixe = Column(Integer)
    id_localisation_mesure = Column(Integer, nullable=False)
    is_exterieur = Column(Boolean, nullable=False)
    is_facade = Column(Boolean)
    is_champ = Column(Boolean)
    hauteur_mesure = Column(Float, nullable=False)
    id_piece_mesure = Column(Integer)
    id_piece_mesure_orient = Column(Integer)
    is_ponctuel = Column(Boolean, nullable=False)
    date_debut = Column(DateTime, nullable=False)
    date_fin = Column(DateTime, nullable=False)
    duree_integration = Column(Float, nullable=False)
    id_materiel_mesure = Column(Integer, nullable=False)
    id_rapport = Column(Integer)
    id_campagne_mesure = Column(Integer)
    id_meteo = Column(Integer)
    commentaire = Column(String)


class LocalisationMesure(Base):
    __tablename__ = "localisation_mesure"
    __table_args__ = {"schema": "mesure_bruit"}
    id_localisation_mesure = Column(
        Integer,
        Sequence(
            "localisation_mesure_id_localisation_mesure_seq", schema="mesure_bruit"
        ),
        primary_key=True,
    )
    geom = Column(Geometry("POINT"))
    commentaire = Column(String)


class MaterielMesure(Base):
    __tablename__ = "materiel_mesure"
    __table_args__ = {"schema": "mesure_bruit"}
    id_materiel_mesure = Column(
        Integer,
        Sequence("materiel_mesure_id_materiel_mesure_seq", schema="mesure_bruit"),
        primary_key=True,
    )
    id_type_materiel = Column(Integer)
    id_marque_materiel = Column(Integer)
    numero_serie = Column(String)


class enumTypeMateriel(Base):
    __tablename__ = "enum_type_materiel"
    __table_args__ = {"schema": "mesure_bruit"}
    id_type_materiel = Column(
        Integer,
        Sequence(
            "enum_type_materiel_id_type_materiel_seq", schema="mesure_bruit"
        ),
        primary_key=True,
    )
    type_materiel = Column(String)


class enumMarqueMateriel(Base):
    __tablename__ = "enum_marque_materiel"
    __table_args__ = {"schema": "mesure_bruit"}
    id_marque_materiel = Column(
        Integer,
        Sequence(
            "enum_marque_materiel_id_marque_materiel_seq", schema="mesure_bruit"
        ),
        primary_key=True,
    )
    marque_materiel = Column(String)


class ResultatMesureBruit(Base):
    __tablename__ = "resultat_mesure_bruit"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_resultat_mesure_bruit = Column(Integer, Sequence('resultat_mesure_bruit_id_resultat_mesure_bruit_seq', schema='mesure_bruit'), primary_key=True)
    id_mesure_bruit = Column(Integer, nullable=False)
    id_nature_mesure = Column(Integer, nullable=False)
    id_indicateur = Column(Integer, nullable=False)
    id_unite_grandeur = Column(Integer, nullable=False)
    id_ponderation = Column(Integer, nullable=False)
    debut_periode = Column(DateTime)
    fin_periode = Column(DateTime)
    is_periode_reference = Column(Boolean)
    valeur = Column(Float, nullable=False)
    incertitude = Column(Float)
    commentaire = Column(String)


class enumNatureMesure(Base):
    __tablename__ = "enum_nature_mesure"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_nature_mesure = Column(Integer, Sequence('enum_nature_mesure_id_nature_mesure_seq', schema='mesure_bruit'), primary_key=True)
    nature_mesure = Column(String, nullable=False)


class enumPonderation(Base):
    __tablename__ = "enum_ponderation"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_ponderation = Column(Integer, Sequence('enum_ponderation_id_ponderation_seq', schema='mesure_bruit'), primary_key=True)
    ponderation = Column(String, nullable=False)


class enumUniteGrandeur(Base):
    __tablename__ = "enum_unite_grandeur"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_unite_grandeur = Column(Integer, Sequence('enum_unite_grandeur_id_unite_grandeur_seq', schema='mesure_bruit'), primary_key=True)
    ponderation = Column(String, nullable=False)


class enumIndicateur(Base):
    __tablename__ = "enum_indicateur"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_indicateur = Column(Integer, Sequence('enum_indicateur_id_indicateur_seq', schema='mesure_bruit'), primary_key=True)
    indicateur = Column(String, nullable=False)


class RelationSourceMesure(Base):
    __tablename__ = "relation_source_mesure"
    __table_args__ = {'schema': 'mesure_bruit'}
    id_source_mesure = Column(
        Integer,
        Sequence("relation_source_mesure_id_source_mesure_seq", schema="mesure_bruit"),
        primary_key=True,
    )
    id_source_de_bruit = Column(Integer, nullable=False)
    id_mesure_bruit = Column(Integer, nullable=False)


class SourceDeBruit(Base):
    __tablename__ = "source_de_bruit"
    __table_args__ = {'schema': 'emission'}
    id_source_de_bruit = Column(
        Integer,
        Sequence("source_de_bruit_id_source_de_bruit_seq", schema="emission"),
        primary_key=True,
    )
    type_source_bruit = Column(Integer, nullable=False)
    commentaire = Column(String)


class enumTypeSourceBruit(Base):
    __tablename__ = "enum_type_source_bruit"
    __table_args__ = {'schema': 'emission'}
    id_type_source_bruit = Column(
        Integer,
        Sequence("enum_type_source_bruit_id_type_source_bruit_seq", schema="emission"),
        primary_key=True,
    )
    type_source_bruit = Column(String, nullable=False)


class TraficRoutier(Base):
    __tablename__ = "trafic_routier"
    __table_args__ = {"schema": "emission"}
    id_trafic_routier = Column(
        Integer,
        Sequence("trafic_routier_id_trafic_routier_seq", schema="emission"),
        primary_key=True,
    )
    id_infra_route = Column(Integer, nullable=False)
    id_source_de_bruit = Column(Integer, nullable=False)
    date_heure_debut = Column(DateTime)
    date_heure_fin = Column(DateTime)
    indicateur = Column(Integer)
    valeur = Column(Float, nullable=False)
    trafic_nature = Column(Integer)


class InfraRoute(Base):
    __tablename__ = "infra_route"
    __table_args__ = {"schema": "emission"}
    id_infra_route = Column(
        Integer,
        Sequence("infra_route_id_infra_route_seq", schema="emission"),
        primary_key=True,
    )
    id_route = Column(Integer, nullable=False)
    largeur = Column(Float)
    nb_voie = Column(Integer)
    declivite = Column(Float)
    sens_circulation = Column(Integer)
    id_rvt_nature = Column(Integer)
    id_rvt_etat = Column(Integer)
    id_rvt_granulo = Column(Integer)
    is_rvt_sec = Column(Boolean)


class enumIndicateurTraficRoutier(Base):
    __tablename__ = "enum_indicateur_trafic_routier"
    __table_args__ = {'schema': 'emission'}
    id_indicateur_trafic_routier = Column(
        Integer,
        Sequence("enum_indicateur_trafic_routier_id_indicateur_trafic_routier_seq", schema="emission"),
        primary_key=True,
    )
    indicateur_trafic_routier = Column(String, nullable=False)


class Route(Base):
    __tablename__ = "route"
    __table_args__ = {"schema": "emission"}
    id_route = Column(
        Integer,
        Sequence("route_id_route_seq", schema="emission"),
        primary_key=True,
    )
    id_nom_route = Column(Integer, nullable=False)
    id_gest_route = Column(Integer, nullable=False)
    id_dept = Column(Integer, nullable=False)
    id_commune = Column(Integer)


class enumSensCircu(Base):
    __tablename__ = "enum_sens_circu"
    __table_args__ = {'schema': 'emission'}
    id_sens_circu = Column(
        Integer,
        Sequence("enum_sens_circu_id_sens_circu_seq", schema="emission"),
        primary_key=True,
    )
    sens_circu = Column(String, nullable=False)

class enumRvtEtat(Base):
    __tablename__ = "enum_rvt_etat"
    __table_args__ = {'schema': 'emission'}
    id_rvt_etat = Column(
        Integer,
        Sequence("enum_rvt_etat_id_rvt_etat_seq", schema="emission"),
        primary_key=True,
    )
    rvt_etat = Column(String, nullable=False)

class enumRvtNature(Base):
    __tablename__ = "enum_rvt_nature"
    __table_args__ = {'schema': 'emission'}
    id_rvt_nature = Column(
        Integer,
        Sequence("enum_rvt_nature_id_rvt_nature_seq", schema="emission"),
        primary_key=True,
    )
    rvt_nature = Column(String, nullable=False)


class enumRvtGranulo(Base):
    __tablename__ = "enum_rvt_granulo"
    __table_args__ = {'schema': 'emission'}
    id_rvt_granulo = Column(
        Integer,
        Sequence("enum_rvt_granulo_id_rvt_granulo_seq", schema="emission"),
        primary_key=True,
    )
    rvt_granulo = Column(String, nullable=False)


class enumNomRoute(Base):
    __tablename__ = "enum_nom_route"
    __table_args__ = {'schema': 'emission'}
    id_nom_route = Column(
        Integer,
        Sequence("enum_nom_route_id_nom_route_seq", schema="emission"),
        primary_key=True,
    )
    nom_route = Column(String, nullable=False)

class enumGestionnaire(Base):
    __tablename__ = "enum_gestionnaire"
    __table_args__ = {'schema': 'emission'}
    id_gest_route = Column(
        Integer,
        Sequence("enum_gestionnaire_id_gest_route_seq", schema="emission"),
        primary_key=True,
    )
    gest_route = Column(String, nullable=False)


class enumCommune(Base):
    __tablename__ = "enum_commune"
    __table_args__ = {'schema': 'emission'}
    id_commune = Column(
        Integer,
        Sequence("enum_commune_id_commune_seq", schema="emission"),
        primary_key=True,
    )
    typecom = Column(String)
    com = Column(String, nullable=False)
    reg = Column(String)
    dep = Column(String)
    ctcd = Column(String)
    arr = Column(String)
    tncc = Column(String)
    ncc = Column(String)
    nccenr = Column(String)
    commune = Column(String, nullable=False)
    can = Column(String)
    comparent = Column(String)

class enumDept(Base):
    __tablename__ = "enum_dept"
    __table_args__ = {'schema': 'emission'}
    id_dept = Column(
        Integer,
        Sequence("enum_dept_id_dept_seq", schema="emission"),
        primary_key=True,
    )
    dept = Column(String, nullable=False)   # The number of characters should be limited to 3
    reg = Column(String)
    cheflieu = Column(String)
    tncc = Column(String)
    ncc = Column(String)
    nccenr = Column(String)
    libelle = Column(String)