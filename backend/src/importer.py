# Module d'import des donnees contenues dans la base
# notamment les prochaines valeur d'identifiants

from sqlalchemy import Sequence


def nextval_id_localisation_mesure(session_instance):
    with session_instance() as session:
        id_loc_next = session.scalar(Sequence('localisation_mesure_id_localisation_mesure_seq', schema='mesure_bruit'))
    return id_loc_next


def nextval_id_mesure_bruit(session_instance):
    with session_instance() as session:
        id_mesure_next = session.scalar(Sequence('mesure_bruit_id_mesure_bruit_seq', schema='mesure_bruit'))
    return id_mesure_next


def nextval_id_resultat_bruit(session_instance):
    with session_instance() as session:
            id_resultat_next = session.scalar(Sequence('resultat_mesure_bruit_id_resultat_mesure_bruit_seq', schema='mesure_bruit'))
    return id_resultat_next


def nextval_id_source_mesure(session_instance):
    with session_instance() as session:
            id_resultat_next = session.scalar(Sequence('relation_source_mesure_id_source_mesure_seq', schema='mesure_bruit'))
    return id_resultat_next

def nextval_id_source_de_bruit(session_instance):
    with session_instance() as session:
            id_resultat_next = session.scalar(Sequence('source_de_bruit_id_source_de_bruit_seq', schema='emission'))
    return id_resultat_next

def nextval_id_trafic_routier(session_instance):
    with session_instance() as session:
            id_resultat_next = session.scalar(Sequence("trafic_routier_id_trafic_routier_seq", schema="emission"))
    return id_resultat_next

def nextval_id_infra_route(session_instance):
    with session_instance() as session:
            id_resultat_next = session.scalar(Sequence("infra_route_id_infra_route_seq", schema="emission"))
    return id_resultat_next

def nextval_id_route(session_instance):
    with session_instance() as session:
            id_resultat_next = session.scalar(Sequence("route_id_route_seq", schema="emission"))
    return id_resultat_next


def nextval_id_users(session_instance):
    with session_instance() as session:
            id_users_next = session.scalar(Sequence('user_id_user_seq', schema='si'))
    return id_users_next
