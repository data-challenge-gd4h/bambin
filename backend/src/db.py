import os

from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker

from .models import User, Base

USERNAME = os.getenv("POSTGRES_USER")
PASSWORD = os.getenv("POSTGRES_PASSWORD")
DATABASE_NAME = os.getenv("POSTGRES_DB")
DATABASE_HOST = os.getenv("POSTGRES_HOST")

engine = create_engine(f'postgresql://{USERNAME}:{PASSWORD}@{DATABASE_HOST}:5432/{DATABASE_NAME}')
Session = sessionmaker(bind=engine)
Base.metadata.create_all(engine)
