from typing import Union
import aiohttp
from json import loads

from fastapi import FastAPI, Request, Form
from fastapi.encoders import jsonable_encoder
from fastapi.responses import HTMLResponse
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy import text, select
import requests


from src.db import Session
from src.models import User, MaterielMesure, enumTypeMateriel, enumMarqueMateriel
from src.pydantic_models import MandatoryFormInput
from src.exporter import Exporter
from src.whitelist import whitelist
from src.importer import nextval_id_users

app = FastAPI()
app.mount("/static", StaticFiles(directory="src/static"), name="static")
templates = Jinja2Templates(directory="src/templates")

token = "fff5cdd4-e239-3119-a19b-a9d541e73622"
# tutorial on Forms: https://eugeneyan.com/writing/how-to-set-up-html-app-with-fastapi-jinja-forms-templates/

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def welcome(request: Request):
    return "Welcome from the backend!"


@app.get("/users")
def get_users():
    with Session() as session:
        result = session.query(User).filter_by().all()
    return result


@app.get("/materiel_mesure")
def get_materiel():
    with Session() as session:
        result = session.execute(
            select(
                MaterielMesure.id_materiel_mesure,
                enumTypeMateriel.type_materiel,
                enumMarqueMateriel.marque_materiel,
                MaterielMesure.numero_serie,
            )
            .join(
                enumTypeMateriel,
                MaterielMesure.id_type_materiel == enumTypeMateriel.id_type_materiel,
            )
            .join(
                enumMarqueMateriel,
                MaterielMesure.id_marque_materiel
                == enumMarqueMateriel.id_marque_materiel,
                isouter=True,
            )
        ).all()
        resultDict = [
            {
                "id_materiel_mesure": b[0],
                "type_materiel": b[1],
                "marque_materiel": b[2],
                "numero_serie": b[3],
            }
            for b in result
        ]
    return resultDict


@app.post("/user")
def create_user(
    firstname, lastname, mail, numero_siren, type_source_bruit=None, is_authorized : bool = False
):
    id_user = nextval_id_users(Session)
    with Session() as session: 
        session.add(
            User(
                id_user=id_user,
                firstname=firstname,
                lastname=lastname,
                mail=mail,
                numero_siren=numero_siren,
                type_source_bruit=type_source_bruit,
                is_authorized=is_authorized,
            )
        )
        session.commit()
        result = (
            session.query(User)
            .filter_by(numero_siren=numero_siren, firstname=firstname, lastname=lastname)
            .first()
        )
    return result


@app.get("/whitelist")
def get_whitelist():
    return whitelist

@app.get('/testRequete')
def get_factice():
    return { "retour" : "coucou me voila"}

@app.get("/siren")
async def get_codeSiren(nomUnitelegale):
    nomUnitelegale = f'"{nomUnitelegale}"'
    async with aiohttp.ClientSession(
        connector=aiohttp.TCPConnector(verify_ssl=False)
    ) as session:
        session.headers.update({"Authorization": "Bearer " + token})
        async with session.get(
            f"https://api.insee.fr/entreprises/sirene/V3/siren?q=periode(nomUniteLegale:{nomUnitelegale})"
        ) as response:
            data = await response.json()
            # print(data['unitesLegales'][0]['siren'])
    return {"siren": data["unitesLegales"][0]["siren"]}


@app.get("/List_Etablissement")
async def get_List_Etablissement(siret):
    dict_etab = {}
    async with aiohttp.ClientSession(
        connector=aiohttp.TCPConnector(verify_ssl=False)
    ) as session:
        session.headers.update({"Authorization": "Bearer " + token})
        async with session.get(
            f"https://api.insee.fr/entreprises/sirene/V3/siret?q=siret:{siret}*"
        ) as response:
            data = await response.json()
            # print(data)
            for i in range(len(data["etablissements"])):
                dict_etab[data["etablissements"][i]["siren"]] = data["etablissements"][
                    i
                ]["periodesEtablissement"][0]["denominationUsuelleEtablissement"]
    return dict_etab


@app.get("/Etablissement")
async def get_Etablissement(siret):
    # siret=f'"{siret}"'
    async with aiohttp.ClientSession(
        connector=aiohttp.TCPConnector(verify_ssl=False)
    ) as session:
        session.headers.update({"Authorization": "Bearer " + token})
        async with session.get(
            f"https://api.insee.fr/entreprises/sirene/V3/siret?q=siret:{siret}"
        ) as response:
            data = await response.json()
            # print(data)
    return {
        data["etablissements"][0]["siren"]: data["etablissements"][0][
            "periodesEtablissement"
        ][0]["denominationUsuelleEtablissement"]
    }


@app.get("/dbcontent")
def get_db_content(schema, table, field):
    if (schema, table, field) not in whitelist:
        raise Exception("Wrong schema or datable or field")
    with Session() as session:
        result = session.execute(text(f"SELECT {field} FROM {schema}.{table}")).all()
    return [r[0] for r in result]


@app.post(path="/submit", response_model=MandatoryFormInput)
def submitted_form(body: MandatoryFormInput):
    # print(body.json())
    # print(body.dict())
    # print(loads(body.json()))
    exporter = Exporter(Session)
    bodyIntegrer = MandatoryFormInput(**loads(body.json()))
    return exporter.export_to_databases(bodyIntegrer)
