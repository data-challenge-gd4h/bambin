# Testing the backend building block locally

- Install Docker or Docker Desktop
- In the project root folder, add a file `.env` with
    ```hs
    POSTGRES_PASSWORD: my_password
    POSTGRES_USER: my_user
    POSTGRES_DB: my_db
    POSTGRES_HOST: my_host
    ```
    where `my_password`, `my_user`, `my_db`, `my_host` are here generic labels to be changed.
- Go to the project root folder and in a console run the command `docker compose up -d --build app`. This creates Docker images. Also, a front form based on FastAPI can be seen in a browser at http://localhost:8080/.
- To test the writing into the database, go to the `submit` form and replace the json by
    ```
    {
    "date_debut": "2023-05-25T08:30:01.734Z",
    "date_fin": "2023-05-25T09:30:01.734Z",
    "hauteur_mesure": 1,
    "maitre_ouvrage": "string",
    "is_ponctuel": true,
    "is_point_fixe": true,
    "is_exterieur": true,
    "id_materiel_mesure": 1,
    "duree_integration": 1,
    "x_coordinate_epsg2154": 300000,
    "y_coordinate_epsg2154": 7000000,
    "comment_localisation": "string"
    }
    ```
    This adds a new line in the table `mesure_bruit.mesure_bruit` (and other related tables).  
    Other input forms can be found in the file `JSON_Test_FastAPI.txt`.

