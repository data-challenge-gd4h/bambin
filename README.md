# Challenge GD4H - BAMBIN

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

Lien : 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a> 

## BAMBIN

L’excès de bruit est un enjeu sanitaire (effet sur l’audition, le comportement, le sommeil), et il provient en majorité des transports, des activités et du voisinage.

Des mesures de bruit sont effectuées par des bureaux d’étude pour le compte d’institutions publiques, mais elles sont difficilement partageables, à la fois d’un point de vue technique et organisationnel (silos par projet).

<a href="https://gd4h.ecologie.gouv.fr/defis/734" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

Initialement, le projet était prévu autour de 3 axes : 
- création d'un modèle de données associé à une base de données relationnelle
- création d'un outil d'alimentation de cette base, sous la forme d'un formulaire de saisie accessible depuis le web.
- création d'un outil de visualisation des données contenues dans la base. 

Chaque axe a été exploré, avec des états d'avancement variables : 
- le [modèle de données](https://gitlab.com/data-challenge-gd4h/bambin/-/tree/main/modele_donnees) a été travaillé en profondeur, et offre une vue assez complète avec : 
    - le [descriptif des observations](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/DescriptionModeleDonnees.md) ayant permis la création du modèle 
    - des diagrammes de classes et entités-relations au format image
    - un [modèle de données format postgresql / postgis](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/Creation_MPD.sql), permettant de créer le squellette de la base de données 
    - un [script de création de données aléatoires](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/Creation_JDDTest.sql), associé aux [données sources nécessaires](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/schema_temporaire.sql). Cf le readme du dossier pour plus de détails
    - un [backup plain text](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/bambin_pgdump.zip) en insertion avec noms de colonne, à restaurer dans une base postgres / postgis, si l'on ne souhaite pas passer par la création de données aléatoires.
- le formulaire de saisie a pu etre testé, en version exploratoire, uniquement sur la partie fourniture de données routière : 
    - le [backend](https://gitlab.com/data-challenge-gd4h/bambin/-/tree/main/backend) est basé sur Python, via docker. 
        - seul un test de la partie route a pu etre réalisé
        - seul un test basé sur une fourniture manuelle a pu être réalisé
        - seul des tests d'insertion ont pu être réalisé. Aucun test de récupération de valeur existante (notamment la géométrie) n'a pu etre réalisé.
    - le [frontend](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/frontend/README.md) a été basé sur Vue et Vite.
        - la documentation n'a pu être complétée entièrement
        - le front crée un object JSON pour l'échange avec le backend
- la visualisation a été testée avec Lizmap, grâce à la participation des membres du bureaui d'étude [altermap](https://altermap.fr/)
    - [exmeple disponible ici](https://cartographie.altermap.fr/index.php/view/map/?repository=bambin&project=bambin)

Pour toute question vous pouvez contacter le porteur de projet : Martin Schoreisz martin.schoreisz@cerema.fr

### **Installation et utilisation**

[Guide d'installation](/INSTALL.md)

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
