CREATE materialized view mesure_bruit.resultat_mesure_bruit_geom AS
	SELECT rmb.id_resultat_mesure_bruit,
		mb.maitre_ouvrage AS siren_maitre_ouvrage,
		ei.indicateur,
		rmb.valeur,
		eug.unite_grandeur,
		rmb.incertitude,
		rmb.debut_periode,
		rmb.fin_periode,
		mb.date_debut AS date_debut_mesure,
		mb.date_fin AS date_fin_mesure,
		lm.geom 
	FROM mesure_bruit.resultat_mesure_bruit rmb 
		INNER JOIN mesure_bruit.mesure_bruit mb ON rmb.id_mesure_bruit = mb.id_mesure_bruit 
		INNER JOIN mesure_bruit.localisation_mesure lm ON mb.id_localisation_mesure = lm.id_localisation_mesure
		INNER JOIN mesure_bruit.enum_indicateur ei ON rmb.id_indicateur = ei.id_indicateur 
		INNER JOIN mesure_bruit.enum_unite_grandeur eug ON rmb.id_unite_grandeur = eug.id_unite_grandeur ; 
