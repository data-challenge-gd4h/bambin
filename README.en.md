# Challenge GD4H - BAMBIN

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a> 

## BAMBIN

Excessive noise is a health issue (effects on hearing, behavior and sleep), and most of it comes from transport, activities and neighborhoods.
Noise measurements are carried out by engineering firms on behalf of public institutions, but they are difficult to share, both technically and organizationally (project silos).

<a href="https://gd4h.ecologie.gouv.fr/defis/734" target="_blank" rel="noreferrer">Find out more about the challenge</a> 

## **Documentation**

Initially, the project was planned around 3 axes:

* creation of a data model associated with a relational database
* creation of a tool for feeding this database, in the form of a web-accessible input form.
* creation of a tool for visualizing the data contained in the database.

Each axis was explored, with varying degrees of progress:

* the [data model](https://gitlab.com/data-challenge-gd4h/bambin/-/tree/main/modele_donnees) has been worked on in depth, and offers a fairly complete view with :

    * [description of the observations](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/DescriptionModeleDonnees.md) used to create the model
        * class and entity-relationship diagrams in image format
        * a [data model in postgresql / postgis format](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/Creation_MPD.sql), for creating the database quilt
        * a [random data creation script](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/Creation_JDDTest.sql), associated with the necessary [source data](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/schema_temporaire.sql). See the folder readme for more details.
        * a [plain text insertion backup](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/modele_donnees/bambin_pgdump.zip) with column names, to be restored in a postgres / postgis database, if you don't want to use random data creation.

* the input form has been tested, in exploratory version, only on the road data supply part:

     * the [backend](https://gitlab.com/data-challenge-gd4h/bambin/-/tree/main/backend) is based on Python, via docker.
         * only a test of the road part could be carried out
         * only a test based on a manual supply could be performed
         * only insertion tests could be performed. No existing value retrieval tests (notably geometry) could be performed.

    * the [frontend](https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/frontend/README.md) was based on Vue and Vite.
         * documentation could not be fully completed
         * the front end creates a JSON object for exchange with the backend

* visualization was tested with Lizmap, thanks to the participation of members of the [altermap design office](https://altermap.fr/)
    * [example available here](https://cartographie.altermap.fr/index.php/view/map/?repository=bambin&project=bambin)


If you have any questions, please contact the project leader: Martin Schoreisz martin.schoreisz@cerema.fr


### **Installation**

[Installation Guide](/INSTALL.md)


### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
