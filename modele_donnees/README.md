## Direct restore
The file `bambin_pgdump.zip` contains a postgresql archive saved in plain text with --columns-insert option. Can be restored in command line by using :  
```
cd yourFolderContainingPsqlExecutable
psql -h yourHost -p yourPort -U yourUser
create database yourDbName ;
\q
psql -h yourHost -p yourPort -U yourUser -d yourDbName -f completePathOfTheFilebambin_pgdump.zip
```
## Step by step creation

### Requirements
- postgres
- postgis extension
- tsm_system_rows extension


### Folder description 

- The folder `data/` contains csv or txt files to create enumeration tables. To be used together with the sql files, it should be renamed as `datas_perso/` and copied-pasted to your postgres folder `your_path/PostgreSQL/your_version/data`.  
- The file `creation_MPD.sql` creates the (main structure of the) database by using files in `data/`.   
- The file `creation_JDDTest.sql` fills in the database (created with `creation_MPD.sql`) with random values. This requires to create first the schema `temporaire`. It also uses files in `data/`.  
- The file `schema_temporaire.sql` can be used to restore the schema   `temporaire`.  
- The files `diagramme_classe.svg` and `diagramme_ERD.svg` are the class and ERD diagrams used to build the database.  

### Known limits and further possible enhancements regarding trial dataset `creation_JDDTest.sql`

Some shortcuts have had to be made in the way we handle dates and periods of time  of the project
Long story short :

 We don't manage heterogeneous periods of time in measurement campaigns. We should be able to separate the way we compute indicators from the way we store what we collect.
 
 We should be able to decide the start and the end of a measurement so as to eventually handle overlapping or gaps in periods of measurement depending on how many devinces are functionning.  See issue #1
 For example in `emission.icpe`, we match `date_heure_debut_obs=mesure_bruit.date_debut` and `date_heure_fin_obs=mesure_bruit.date_fin`. and that is not completely exact.  see Issue #1
 
- Note that in the table `mesure_bruit.mesure_bruit`, there are only fixed points (e.g. with `is_point_fixe=true`).  see issue #2



- Note there are weird entries (`Whisky`, `Yankee`) in the table `emission.enum_sens_circu` . Not yet corrected. see issue #3 

- We should also be able to invalidate some measures when specific case are caught . Refer to issue #4

- In the table `mesure_bruit.resultat_mesure_bruit`, I set `debut_periode=Null` and `fin_periode=Null` when `is_periode_reference=true`. But I guess this should be modified by giving the actual reference periode e.g. 6h-22h. See issue #5
- In the table `emission.trafic_routier`, A mix of  `indicateur`  and `date_heure_debut` `date_heure_fin` appears because and indicator is set upon a timespan of a day ( open hours, nightime 6am-10pm for instance). Those indicator should only consider the correct timebox and skip out-of-frame data.  See issue #5
 
 
 












