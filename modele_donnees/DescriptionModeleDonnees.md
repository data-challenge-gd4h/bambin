<span style="font-weight:700;font-size:24px"> 
Description verbale des mesures de bruit
</span>
<br>
  
Ce document vise à fournir une première version d’un modèle de données des mesures de bruit dans l’environnement.  
il sert de base à l’élaboration d’un schéma de données permettant aux maîtres d’ouvrage et / ou maître d’œuvre de mesures acoustiques normalisée de diffuser des résultats pouvant facilement être mis à disposition.
# ***Le contexte***
Afin de pouvoir centraliser différentes mesures de bruit réalisées sur un même territoire, Bordeaux Métropole et le Cerema souhaitent se doter d’un modèle de données permettant de stocker les mesures de bruit réalisées et à venir.  
Attention, il ne s’agit pas de fournir un cadre d’analyse des niveaux de bruit mesurés au regard de la réglementation, mais de définir un cadre commun pour les données créées lors de mesures de bruit.
À noter que les systèmes d'information par [BruitParif](https://rumeur.bruitparif.fr/) et [Acoucité](https://www.acoucite.org/observatoire/reseau-de-mesures/reseau-permanent-de-mesure/) doivent reposer sur des modèles de données qui leurs sont propres. Ces deux associations ont été contactées.

 
# ***Qu’est-ce qu’une « mesure de bruit » : modèle conceptuel***
## ***Le domaine d’application***
Une mesure de bruit est, intrinsèquement, réalisable par n’importe qui (particulier, professionnel, personne morale), avec un éventail de capteur varié, (sonomètre, microphone + enregistreur, smartphone, etc), en tout point de l’espace (intérieur, extérieur, dans l’air, dans l’eau, etc...). Dans le cadre du présent modèle, les cas étudiés se limitent :  
- aux mesures réalisées par l’État ou l’un de ses établissements publics, par les bureaux d’études privés ou les collectivités territoriales,
- aux mesures caractérisant au moins l’une des sources de bruit suivantes : 
- bruit routier,        
- bruit ferroviaire,
- bruit aérien,
- bruit industriel.  

Les protocoles de réalisation de ces mesures sont décrits par des normes françaises, respectivement : 
- NF S 31-085 « Acoustique - Caractérisation et mesurage du bruit dû au trafic routier - Spécifications générales de mesurage »
- NF S 31-088 « Acoustique - Caractérisation et mesurage du bruit dû au trafic ferroviaire »
- NF S 31-110 « Acoustique - Caractérisation et mesurage des bruits de l'environnement - Grandeurs fondamentales et méthodes générales d'évaluation »
- NF S 31-120 « Acoustique - Caractérisation et mesurage des bruits de l'environnement - Influence du sol et des conditions météorologiques »
- NF S 31-010 « Acoustique - Caractérisation et mesurage des bruits de l'environnement - Méthodes particulières de mesurage »
Certains arrêtés fournissent également des définitions précises relatives aux méthodes de mesurage : 
- L’arrêté du 20 juillet 2004 relatif aux dispositifs de mesure de bruit et de suivi des trajectoires des aéronefs
- L’arrêté du 23/01/97 relatif à la limitation des bruits émis dans l'environnement par les installations classées pour la protection de l'environnement (pour les installations classées modifiées ou mise en service apres juillet 1997)
- L’arrêté du 20 août 1985 relatif aux bruits aériens émis dans l'environnement par les installations classées pour la protection de l'environnement (pour les installations classées avant la mise en service ou modification avant le 1er juillet 1997).
Il s’agit donc de s’appuyer sur ces textes pour définir un modèle de données qui permette de centraliser toute mesure de bruit réalisée dans le cadre de ces normes.

**Ce modèle de données ne vise pas à décrire des relations entre mesures de bruit et niveaux réglementaires**, mais uniquement à proposer une standardisation des données contenues dans les mesures de bruit. Les outils législatif et réglementaires d’exploitation des résultats de mesures sont décrit en annexe.
## ***Définitions***
### ***Définitions générales***
- <ins>Maître d’ouvrage et maître d’œuvre : </ins>Dans le présent travail, une mesure de bruit est considérée comme réalisée pour le compte d’un maître d’ouvrage, par un maître d’œuvre. Le maître d’ouvrage et le maître d’œuvre peuvent être la même entité. Un maître d’ouvrage peut faire réaliser plusieurs mesures, un maître d’œuvre peut réaliser plusieurs mesures. Il n’existe qu’un seul maitre d'ouvrage et qu'un seul maître d’œuvre par mesure. Les maîtres d’ouvrage et d’œuvre sont des entités publiques ou privés décrites par des identifiants nationaux (SIREN / SIRET). Potentiellement ils pourraient également être des particuliers (mais pas dans les cas présentement étudiés).<br>
- <ins>Campagne de mesure : </ins>Une mesure peut faire partie d’une campagne de mesure. Une campagne de mesure est un regroupement de plusieurs mesures.<br>
- <ins>Rapport de mesure : </ins>Une mesure peut faire l’objet ou non d’un seul rapport (format pdf) et ou plans de situations. Une mesure de bruit peut faire l’objet de commentaires.<br>
- <ins>Localisation et géométrie : </ins>Une mesure de bruit est réalisée par un appareil de mesure ou une chaîne de mesure en un point de l’espace unique. Un point de l’espace peut supporter plusieurs mesures (cas de mesures dites « avant – après » par exemple). Ce point peut être dans ou à l’extérieur d’un bâtiment. Lorsque le point est à l’extérieur, la mesure peut être réalisée en façade d’un bâtiment (à 2 mètres ou moins de la façade) ou en champ libre, mais n'est pas décrite par une pièce du bâtiment et son orientation. Dans les deux cas, la hauteur de l’appareil de mesure par rapport au sol doit être fournie, ainsi que sa position selon le système de projection légal en vigueur sur le territoire concerné. Dans le cas de mesure en intérieur, la description de la pièce de mesure est obligatoire et se fait par la description du type de pièce et de son orientation parmi une liste finie de choix (exemple : pièce = chambre et orientation = Sud-Ouest) ; en cas d’immeuble, il existera une géométrie pour chaque logement où à lieu une mesure (la géométrie sera placée au maximum aux coordonnées géomatiques de l’emplacement du matériel de mesure de bruit). Exemple : une mesure est réalisée au 4ème étage dans une chambre, une autre au 6 ème étage dans un salon, au sein d'un immeuble de 10 étages. il existera 2 géométries, aux lieux de mesures, avec renseignement des situation propres à chaque logement ayant fait l'objet d'une mesure (cas 1 : hauteur = 11m, pièce = chambre, orientation = Nord-Ouest ; cas 2 : hauteur = 16m, pièce = salon, orientation = Ouest).<br>
En vertu de la RGPD, une mesure ne peut être décrite par une référence à une personne ou une adresse.<br>
- <ins>Appareil de mesure : </ins>L’appareil de mesure est décrit par son type (liste finie, exemple sonomètre classe 1, sonomètre classe 2, chaîne de mesure spécifique, ...), sa marque, son numéro de série, le rapport d’étalonnage le cas échéant. Un même appareil de mesure peut être utilisé lors de plusieurs mesures. Une mesure est effectuée par un appareil de mesure (dans la cas de mesures permanentes, si l’appareillage change, on recrée une nouvelle mesure). <br>
- <ins>Caractérisations temporelles de la mesure, prélèvement et point fixe : </ins>Une mesure est effectuée par un capteur permanent ou un capteur ponctuel (i.e le capteur enregistre en continu tout au long de l'année, ou il est posé spécifiquement sur une durée limitée). Dans tous les cas, une mesure est caractérisée par sa durée date et heur de début et de fin). Une mesure est caractérisée par la durée d’intégration, c’est-à-dire l’intervalle de temps minimal de restitution d’un niveau sonore par l’appareil de mesure. Cette durée d’intégration est généralement de 0,125 seconde ou 1 seconde.  
Selon les mesures considérées (bruit routier NF S31-085 ; bruit ferroviaire NF S31-088) une mesure peut être caractérisée par sa nature : point fixe (i.e mesure réalisée sur 24 heures) ou prélèvement (mesure d’une durée plus courte -généralement 30 minutes ou 1 heure). Un prélèvement est obligatoirement associé à un et un seul point fixe. Un point fixe peut avoir 0 ou plusieurs prélèvements associés. Un prélèvement possède une localisation et des caractéristiques de situation (hauteur, pièce, orientation de la pièce) qui lui sont propres. Les normes relatives aux mesures de bruit aérien et industriel ne prévoit pas la réalisation de prélèvement.<br>
- <ins>Conditions météorologiques et de propagation : </ins>Une mesure de bruit est réalisée en présence de phénomènes météorologiques qui peuvent être identifiés. Le sol entre une source et un point de mesure peut etre décrit par 0, un ou plusieurs statut, selon le nombre de configuration existantes (béton sur la moitié e la distance source - mesure, puis herbe sur le reste par exemple) . <br>
- <ins>Sources de bruit : </ins>Une mesure de bruit est caractérisée par une ou plusieurs sources de bruit prédominantes.<br>
- <ins>Résultats : </ins>Les résultats d'une mesure peuvent etre multiples : 
    - décomposition par période plus fine que la période de mesure
    - fourniture de plusieurs indicateurs.  <br>

Pour information (car non pris en compte dans le présent modèle):
- <ins>Données brutes : </ins>Une mesure produit des données brutes, qui correspondent à un ou plusieurs indicateurs, mesurés à chaque intervalle de temps équivalent à la durée d’intégration, depuis le début jusqu’à la fin de la mesure. Toutefois, ces données peuvent être impactées par des bruits parasites, et nécéssite toujours une analyse à posteriori par des acousticiens. Dans le présent contexte, ces données sont jugées peu pertinentes et donc exclues.  
Ils peuvent être accompagnées de la décomposition en octave ou tiers d’octave.
### ***Conditions de propagations***
#### ***Météorologie***
Au cours des mesures acoustiques, les conditions météorologiques influent sur la forme des rayons acoustiques. Une mesure est optionnellement associée à une description des conditions météorologiques. Une description de conditions météorologiques est obligatoirement associée à une mesure. 

La météorologie constatée lors d’une mesure de bruit est soit de type conventionnelle (i.e elle respecte les préconisations de la norme NF S 31-110), soit de type spécifique (elle ne respecte pas les préconisations pré-citées). En théorie, des mesures ne sont menées que dans des cas de conditions météo conventionnelles, mais des exceptions peuvent exister.
Les conditions météorologiques sont qualifiées selon une méthode qualitative ou quantitative. Selon la méthode utilisée, les paramètres décrivant les conditions météorologiques varient (par exemple la qualification qualitative exige d’observer la couverture nuageuse, ce qui n’est pas nécessaire avec la qualification quantitative, qui ne repose que sur l’observation des températures, orientation et vitesses de vent, cf ci-après).
La météo peut être recueillie soit par observation locale, soit par station météo fixe, soit par dispositif météo mobile. Les stations fixes ou dispositifs météos mobiles sont décris par leur localisation et une description sommaire. Dans le cas de station météo-France, la liste des stations météo fixes principales de météo france est disponible sur le web3.
Les indicateurs mesurés varient selon le type de recueil : dans le cas d’un recueil par station météo les indicateurs collectés sont parmi le rayonnement (ou couverture nuageuse), l’humidité du sol, la vitesse du vent, la direction du vent, heure de lever et de coucher de soleil, la température, la direction et la vitesse du vent, à une ou deux hauteurs différentes. Dans le cas d’observation, les indicateurs sont codés par une liste de valeur finie de la norme NF S31-120 et mélange plusieurs sources de phénomène (vent et rayonnement par exemple)

Chaque mesure est caractérisée par les évolutions météorologiques durant la période qu’elle couvre. Chaque évolution météorologique est décrite selon un ou plusieurs indicateurs. Chaque évolution météo est relative à un intervalle de temps. Cet intervalle est borné par sa date et heure de début et de fin.
#### ***Nature et état des sols***
Pour les sources de bruit terrestres, la nature des sols peut impacter fortement les niveaux de bruit, selon la distance à la source. Chaque source de bruit peut faire l'objet de la description du sol sur l’espace compris entre elle et le point de mesure. Chaque espace ainsi décrit peut être composé d’une ou plusieurs références aux zones homogènes en termes de natures de sol, états de sol et pourcentage de distance couverte par rapport à la distance totale source-récepteur.
### ***Sources de bruit observées***
Dans le cadre du présent modèle de données, 4 type de sources de bruit sont considérées : 
- route
- voie ferrées
- industrie (ICPE), activités artisanales, site soumis a déclaration...
- aviation commerciale et autres (hélicopteres.aviation légère)
Une source de bruit est caractérisée par son type. Selon le type, des caractéristiques descriptives additionnelles sont ajoutées. Une mesure est obligatoirement impactée par au moins une source de bruit. Une source de bruit peut impacter une ou plusieurs mesures. Il est possible qu’une mesure soit réalisées sans connaître les caractéristiques d’émission (hormis le ou les types de sources).
#### ***Routes et voies ferrées***
Le bruit émis par les routes et voies ferrées est relatif à la fois au trafic supporté par l’infrastructure au moment de mesures et à certaines caractéristiques de l’infrastructure variables dans le temps (revêtement de chaussées, type de traverse, etc..). C’est donc la combinaison de paramètres de trafic et d’infrastructure qui décrit ces sources de bruit. Les paramètres de trafic varient sur des temps courts (heure, journée), les paramètres d’infrastructures varient sur des temps longs (années).
Les caractéristiques physiques de ces infrastructures sont (beaucoup de ces caractéristiques sont des listes finies): 
- route : 
    - largeur
    - nombre de voies
    - déclivité
    - revêtement de chaussée : 
        - nature
        - état de surface
        - hygrométrie
    - protections existantes (1 infrastructure routière est relative à une seule route, mais peut être bordée par plusieurs protections)
        - dimensions
        - natures
    - sens de circulation (sens unique ou double sens)
- voie ferrée :
    - nombre de voies
    - déclivité
    - position au sol
    - point singulier
    - type de pose :
        - revêtement
        - traverses
        - rails.  

À noter que le Cerema dispose d’une base de données non exhaustive des protections acoustiques.

Les caractéristiques du trafic routier sont la vitesse et le nombre de véhicules, pour chaque classe de véhicules.  
Les principales classes de véhicules sont: 
- véhicules légers, 
- Poids Lourds, 
- Tous véhicules

Les principaux indicateurs de vitesses sont : 
- vitesse moyenne, 
- quantiles de vitesses (V85 pour la vitesse maximale en dessous de laquelle roule 85% des véhicules, V50 pour la médiane, etc)

Les indicateurs de trafic dépendent de la période de temps considérées. À l’échelle d’une journée complète, les indicateurs de trafic moyen journalier sont utilisés :
- Trafic Moyen Journalier Annuel (tout jours confondus), 
- TMJ Ouvrés (du lundi au dimanche), 
- TMJ Été, 
- TMJ Hors Été…, 
- pourcentage de poids lourds pour chaque situation : 
    - moyenne annuelle
    - jours ouvrés, etc.  

Sur une période de temps inférieure, les indicateurs utilisés sont des débits selon le type de véhicules.<br>

Les indicateurs de trafic sont fournir par périodes, décrites par des horodates de début et de fin.<br>

Le trafic des voies ferrées est relatif aux nombre et type de trains qui circulent, par période de temps (trafic moyen journalier ou débit). Comme pour les débits routiers, si un indicateur moyen journalier est fourni, la référence à une horodate de début de la mesure n’est pas nécessaire.  
Les trains peuvent être classés dans de grande catégorie (Fret, TGV, TER, etc). Un train est lui même une composition de matériel roulant, caractérisé par leur type et leur nombre (1 locomotive de type XXX + 3 wagons de type YYY par exemple). Chaque train roule à une vitesse fonction de la nature du matériel roulant et de la voie (la vitesse du train est limité par les deux paramètres). Chaque train (et donc l’ensemble du trafic) circule sur un sens donnée (connu ou non), sur une voie donnée (connue ou non).  
Il est possible que l’opérateur réseau ou la SNCF dispose déjà d’un modèle de donnée et / ou de listes de valeurs possibles pour décrire le matériel roulant. À noter qu’un même matériel peut rouler à des vitesses différentes selon l’infrastructure qu’il parcourt.

Important : Une mesure de bruit routier ou ferroviaire peut décrire la situation lors de la mesure (niveau de bruit « de constat »), mais peut également décrire une situation moyenne, voir prédictive (niveau de bruit « de long terme ») en se basant sur des trafics autres que ceux du jour de mesure (et grâce à des formules de calcul normalisées).   
Un même indicateur peut donc prendre plusieurs valeurs différentes selon le type de niveau de bruit (« de constat » ou « long terme ») considéré.  
#### ***Bruit industriel***
Le bruit industriel est relatif à une Installation Classée pour la Protection de l’Environnement, dont la liste est finie (définies via la nomenclature mentionnée à l’article L. 511-2 du code de l’environnement, et annexée à l’article R. 511-9 du même code). Chaque ICPE peut être décrit par son numéro Siret ou Siren. De plus, une ICPE peut faire l’objet d’un ou plusieurs arrêtés d’exploitation (cf Géorisque?).
Le bruit industriel est caractérisé par une hétérogénéité extrême entre les différentes ICPE. Ainsi, il est délicat de définir des indicateurs descriptif de la source de bruit, notamment pour :
- la durée du (des) bruit(s) particulier(s)
- le(s) moment(s) de la journée où le(s) bruit(s) se manifeste(nt)
- les conditions de fonctionnement de la (des) source(s) de bruit telles qu'elles ont pu être appréhendées.

De ce fait, la description de la source repose sur les éléments rapportés par l’opérateur en charge de la mesure.
Une ICPE peut être contrôlée plusieurs fois tout au long de son exploitation, et ses caractéristiques peuvent évoluée dans le temps.
#### ***Bruit aérien***
Les mesures de bruit aérien sont réalisées par des dispositifs permanents, validés par l’ACNUSA, sur une liste d’aéroport définie. Une source de bruit est donc caractérisée par l’aéroport en question.  
Le nombre, les trajectoires de passage et l’identification des aéronefs sont fournis par le système d’acquisition des données de suivi des trajectoires et d’identification des aéronefs associé au système de mesure du bruit.<br>Un système d’acquisition des données de suivi des trajectoires peut être associé à une ou plusieurs mesures du bruit, mais une mesure de bruit est associée à un seul système d’acquisition des trajectoires et identification des aéronefs.<br>
Une source de bruit aérien est décrite par 0 ou plusieurs passages d'avion. Chaque passage est associé à une horodate. Chaque avion peut, ou non, être décrit par son modèle, sa famille, son identifiant unique international et toutes les caractéristiques nécessaires d’un point de vue acoustiques. Chaque passage peut être associé à zéro, un ou plusieurs point décrivant la trajectoire du passage (i.e plusieurs point de l’espace (3 dimensions) et du temps).
## ***Résultats d’une mesure de bruit***
Chaque mesure produit un ou plusieurs résultats. Les résultats peuvent être associés :
- à une période de temps fini, descriptive d’une situation particulière et décrite (comme pour le trafic ou la météo) par une date et heure de début et la date et heure de début de la période suivante,
- à une période de référence, mentionnée par la réglementation.<br>

Un indicateur est décrit par la période de temps qu'il couvre, éventuellement par une pondération fréquentielle (A, Z ou C par exemple) et / ou une décomposition par bande de fréquence(octave ou tiers d’octave).<br>
Chaque indicateur est décrit par une valeur et une unité de mesure.<br>
Les résultats sont également décrits par leur nature, qui correspond à la nature de la mesure effectuée : mesure de constat, de long terme ou long terme météorologique. Un résultat de long terme est associé à des caractéristiques de sources de bruit de long terme et possiblement à des caractéristiques météos de long terme. Un résultat de constat peut ne pas être associé à des caractéristiques de sources de bruit et / ou de météo.<br>
Un résultat peut, ou non, être associé à une incertitude qui dépendra entre autres, de la chaîne de mesure employée et des conditions de mise en œuvre. L’incertitude est directement liée au résultat : même dates de début et fin, période de référence, pondération fréquentielle et unité de mesure.
## ***Données brutes***
Les données brutes étaient initialement prévues dans le modèle de données, mais en ont été exclues, notamment à cause de la raison ci-dessous. Le présent chaitre est donc présenté à titre purement informatif.<br>
> Les données brutes sont définies pour un horodatage précis (date et heure avec millième de seconde). Pour un même horodatage, plusieurs indicateurs et plusieurs pondérations peuvent être disponibles. Chaque information brute peut être associée ou non à un ensemble de niveau de bruit relatif à chaque bande d’octave ou de tiers d’octave composant le niveau sonore mesuré à l’instant considéré.<br>
**ATTENTION** !!! En acoustique les données brutes sont souvent vérifiées et post-traitées, de par la présence de bruits parasites non représentatifs ou de codages de suppression que mettrait en place l’opérateur avant le calcul des niveaux agrégés. Il faut donc être vigilant sur ces données : elles ne reflètent pas toujours les niveaux sonores finalement transmis par le maître d’œuvre
## ***Mise en œuvre du modèle de données***
Les normes et textes réglementaires proposent la saisie d’une quantité d’informations importantes, allant de l’hygrométrie du sol jusqu’au détail précis de chaque composant d’un sonomètre de mesure. Toutes ces informations ne sont pas nécessaires pour la visualisation de résultats de mesures. Mais puisque les textes normatifs les mentionnent, le présent modèle les propose également. Cependant, le caractère obligatoire ou non est détaillé dans le Modèle physique de données.
# ***Modèle physique de données***
Après la description du concept, le modèle physique va permettre de détailler l’implémentation. Il permettra notamment de fixer les contraintes de nullité, d’unicité ou de vérification. Les index ne seront pas détaillés ici, car jugé du ressort d’informaticiens.
Il est important de garder à l’esprit que ce modèle doit permettre de stocker des données passées et futures. Si les données futures peuvent être maîtrisées, les données passées s’imposent aux concepteurs du présent modèle.<br>
Par défaut, la « TIME ZONE » de la base de données doit être celle de Paris.
## ***Partie mesures de bruit***
Le maître d’ouvrage d’une mesure doit être connu, mais le maître d’œuvre peut avoir été oublié. La localisation d’une mesure est obligatoirement connue, comme son type (Point fixe (valeur par défaut) ou prélèvement), son statut en exterieur ou interieur d’un bâtiment (par défaut une mesure est en exterieur) sa hauteur, sa temporalité (Ponctuelle (valeur par défaut) ou permanente), sa durée d’intégration (par défaut cette durée est d’une seconde, cette valeur est toujours positive) et la nature du matériel de mesure (par défaut c’est un sonomètre de classe 1). Si une mesure est en interieur d'un bâtiment, elle n'est pas en façade ni en champ libre et la pièce et son orientation doivent être connus. Si la mesure est en exterieur d'un bâtiment, elle est soit en facade soit en champ libre (par défaut une mesure est en façade) et la pièce et son orientation ne sont pas renseignées. La hauteur de la mesure est forcément positive.<br> 
La marque et le numéro de série du matériel son optionnel, mais doivent être uniques si connu. Le numéro de série ne peut pas être présent sans la marque.<br>
Les résultats d’une mesure sont caractérisé par une date et heure de début et une date et heure de fin. La période couverte par ces dates et heures peut être une période de référence. Chaque résultat correspond à une seule combinaison de nature, indicateur, pondération, date et heure de début, date et heure de fin. Les valeurs par défaut sont : 
- pondération : A
- indicateur : Leq
- unité : décibel

Pour chaque résultat décrit par une décomposition fréquentielle, il n’existe qu’une seule combinaison de nature de bande et fréquence centrale. Les bandes d’octave ne peuvent être associées qu’aux fréquences centrales de valeur 31, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000.<br>
Les valeurs des résultats ou de la décomposition fréquentielles sont toujours positives.<br>
Une relation source – mesure est unique.<br>
La fin d’une mesure est obligatoirement postérieure à son début.  

## ***Partie météorologie et propagation***
Chaque ligne de la table météo correspond à une mesure de météo. Par défaut le type de condition météo est ‘conventionnelle’. Chaque combinaison de mesure (ou observation), horodate, indicateur et hauteur doit être unique pour une même station météo. Chaque combinaison de type de station, description et géométrie de station météo est unique. La hauteur d’une mesure météo est forcément positive (car exprimée par rapport au sol). La valeur d’une mesure météo est forcément positive, sauf en cas de mesure de température.<br>Si le sol est décrit, sa nature et le pourcentage d’occupation de cette nature sur l’ensemble du trajet source - récepteur doivent être décris. Ce pourcentage est compris entre 1 et 100 inclus (100 par défaut). Chaque combinaison de trajet source – récepteur, nature de sol et pourcentage d’occupation est unique. En cas de neige, l’épaisseur est forcément positive.<br>  
## ***Partie sources de bruit***
Une source de bruit correspond à une émission sur une période de temps finie (correspondante à la caractérisation de son impact par une ou plusieurs mesures). La connaissance des sources de bruit implique peu d’incertitude, donc une majorité des données doit être connue.
### ***Bruit industriel***
Une source de bruit est associée à une icpe. Une icpe peut faire l’objet de plusieurs sources de bruit (caractérisée plusieurs fois dans le temps).
### ***Bruit aéroportuaire***
Une circulation aérienne correspond à un avion. Son identifiant unique international peut etre connu ou non, mais l’avion est obligatoirement rattaché à un aéroport et à une horodate. La combinaison avion, aéroport, horodate est donc unique. Si la trajectoire de cet avion est connue, elle est décrite par un ensemble de points (géométrie x, y, z dans le système de projection adapté) tous horodatés.
### ***Bruit ferroviaire***
Un numéro de ligne peut suffire à décrire une voie ferrée, au sens d’un objet immatériel référencé par le gestionnaire de réseau. Les noms communs ne servent qu’à fournir un descriptif plus « human readable ». Les infos d’infrastructure sont un plus mais non essentielles. Le matériel roulant et sa composition peuvent être dupliqués entre plusieurs sources de bruit, si les conditions de trafic sont les mêmes.<br>
Le trafic ferroviaire correspond à une unique combinaison de l’infrastructure, sens de circulation, horodate, indicateur et nature.<br>
La vitesse, le nombre de passage et le nombre de module sont forcément positifs. Si l’indicateur de trafic est de type TMJ, l’horodate du trafic doit être nulle.
### ***Bruit routier***
Les routes numérotées (autoroutes départementales, nationales, régionales) sont décrites par une combinaison unique de dénomination, gestionnaire et département. Pour ces routes, la commune n’est pas obligatoire. Les voies communales ou métropolitaines sont décrites par leur nom, gestionnaire, département et commune concernées. Une combinaison de nom, gestionnaire, département et commune est unique.<br>
La largeur et le nombre de voie sont supérieurs à 0. la combinaison des paramètres d’une infrastructure donnée est unique. Une déclivité montante s’exprime avec une valeur positive, une déclivité descendante avec une valeur négative.<br>
La combinaison entre une infrastructure, une horodate, un sens de circulation, un indicateur de trafic et une nature de trafic est unique. Une valeur de trafic routier est forcément positive.
# ***Spécifications de saisie du jeu de données test***
Ces spécifications détaillent le processus de saisies des données du jeu de test, dont la représentativité de la réalité n’est pas assuré (l’objectif des données test est surtout de respecter le modèle physique de données).<br>
Le choix est fait de démarrer par le renseignement des caractéristiques météo.
## ***La météorologie***
Pour générer une météo, il est nécessaire de créer : 
1. le descriptif général de la météo (table « propagation.meteo »)
2. le cas échéant, la station météo (table « propagation.station_météo »)
3. les mesures et / ou observations (tables propagation.mesure_station_meteo et / ou propagation.observation_meteo »)
## ***Mesures de bruit***
En premier lieu, il s’agit de s’assurer que toutes les clés référencées par une mesure sont définies. Cela concerne : 
- la localisation des mesures, 
- les matériels de mesure,
- les tables d’énumération
### ***Localisation des mesures***
Les géométries sont générées aléatoirement dans la table « mesure_bruit.localisation_mesure » sur la surface couverte par les communes composant Bordeaux Métropole.
### ***Le matériel de mesure***
Dans le présent exemple, des matériels avec ou sans marque et / ou numéros de série sont générés. Les seuls type de matériels implémenté sont les sonomètres (classes 1 ou 2) et les chaînes de mesures personnalisées.
### ***Les tables d’énumération***
Presque toutes les tables présentent des valeurs maîtrisées, hormis la table des parties prenantes, qui peut nécessiter un ajout. Dans le cadre de la création d’une base factice, 8 nouvelles valeurs ont été ajoutées par rapport au script de création du Modèle Physique de Données.
### ***Création des mesures***
les mesures présentent beaucoup de variations possibles, dont les principales sont : 
- séparation des points fixe et des prélèvements
- maîtrise d’ouvrage connu et maîtrise d’œuvre connu, maîtrise d’ouvrage connu et maîtrise d’œuvre inconnu
- localisation de mesure partagée ou non
- comptages permanents ou ponctuels
- au moins une mesure en intérieur, avec descriptif de la piece et de l'orientation
- quelques mesures en champ libre, d'autres en façade
- si la météo est connue, les dates de mesure et de météo coïncident
- durée d’intégration de 0.125 ou 1 seconde
- météo présente ou non

Les données créées tentent de répondre à un maximum de cas possible.
## ***Sources de bruit***
La création des sources passe par : 
    1. la création de la source et de son type dans la table « emission.source_de_bruit »,
    2. l’affectation des émissions à une ou plusieurs mesures de bruit
    3. l’affectation des émissions à la source créée.
Pour affecter les sources aux mesures, on crée des cas avec plusieurs sources pour une mesure, et plusieurs mesures pour une source.
### ***ICPE***
La table est simple, et directement construite à partir d’une agrégation aléatoire des sources de bruit à des ICPE. Un doublon de numéro siret est gardé pour l’exemple.
### ***Source aérienne***
La première étape consiste à créer la table des appareils aériens, puis de simuler des circulations en affectant plusieurs appareils à des temps de passage différents. Les temps de passages doivent correspondre aux temps de mesure de bruit. Une trajectoire est simulée à partir d’une ligne tracée manuellement.
### ***Source ferroviaire***
La table des voies ferrées est issue de la base de données PlaMADE du Cerema. Elle doit être construite en premier.
Pour les infrastructures, des cas d’infrastructures multiples ou uniques par voies ferrées sont créés.
Le trafic présente de nombreuses possibilités, dues aux variations possibles de : 
- nature de trafic (constat ou long terme)
- indicateurs (TMJA ou débits)
- sens de circulation (une donnée par sens ou deux sens confondus)
- nature du trafic (long terme ou constat)

Des cas sont générés pour chaque situation ci-dessus, mais l’exhaustivité des croisements de situation n’est pas garantie.
Les matériels roulants sont décris aléatoirement, sans garantie de présence de l’intégralité des identifiants de trafic ferroviaire dans la table des matériels. De même entre la table du matériel roulant et celle des compositions de matériels : les identifiant de matériels ne sont pas obligatoirement présents dans la table des compositions.
### ***Bruit routier***
la création des routes et infrastructure routières ne répond à aucune logique de réalité. De même pour les affectations de trafic, ou le seul de but est de mettre ne valeur les différences entre les attributs moyens journaliers et les attributs horaires. Les vitesses sont considérées comme des attributs horaires.
### ***Reste des éléments***
A définir...
# ***Gestion de la Base de données et du SI***
## ***Controle des utilisateurs***
La gestion et le contrôle des utilisateurs se font dans un premier temps par stockage des infos de connexions au sein du schéma `si` dans la table `user`. les infos de connexions sont : 

- le nom (obligatoire ; texte de taille variable)
- le prénom (obligatoire ; texte de taille variable)
- l'adresse email (obligatoire ; texte de taille variable ; unique)
- le numero de siren (facultatif ; texte de taille variable)
- le type de source préférentielle (facultatif ; liste énumérée vers le type de source)
- l'autorisation (booleen ; obligatoire ; par défaut False)

# ***ANNEXE***
## ***Textes législatifs et réglementaires***
### ***Bruit Routier***
- Décret n° 95-22 du 9 janvier 1995, relatif au bruit des infrastructures de transport terrestre,
- Arrêté du 5 mai 1995, relatif au bruit routier,
- Arrêté du 30 mai 1996, relatif aux modalités de classement des infrastructures de transports terrestres et à l’isolement acoustique des bâtiments d’habitation dans les secteurs affectés par le bruit.
### ***Bruit Aéronef et hélicoptères***
- La loi n° 92-1444 du 31 décembre 1992 relative à la lutte contre le bruit a institué un dispositif d’aide à l'insonorisation des logements riverains des dix plus grands aérodromes nationaux (Roissy, Orly, Marseille, Nice, Toulouse, Lyon, Bordeaux, Strasbourg, Mulhouse et Nantes). 
- Décret n° 2011-1948 du 23/12/11 relatif à l’aide à l’insonorisation des logements des riverains des aérodromes mentionnés au I de l’article 1609 quatervicies A du code général des impôts
- Décret n° 2010-1226 du 20/10/10 portant limitation du trafic des hélicoptères dans les zones à forte densité de population
- Décret n° 2010-405 du 27/04/10 relatif à la procédure devant l'Autorité de contrôle des nuisances sonores aéroportuaires (ACNUSA) statuant en matière de sanctions
- Décret n° 2009-647 du 09/06/09 relatif à l'aide à l'insonorisation des logements des riverains des aérodromes mentionnés au I de l'article 1609 quatervicies A du code général des impôt
- Livre V : Prévention des pollutions, des risques et des nuisances - Titre VII : Prévention des nuisances sonores (partie réglementaire)
- L’Arrêté du 20 juillet 2004 relatif aux dispositifs de mesure de bruit et de suivi des trajectoires des aéronefs ( si je me trompe pas c’est totalment géré par les les aéronefs eux mm et acnusa)
### ***Bruit des activités industrielles (soumis à autorisation)***
- L’Arrêté du 23/01/97 relatif à la limitation des bruits émis dans l'environnement par les installations classées pour la protection de l'environnement  (en partie, tt les sites industriels ne sont pas gérées comme des ICPE et donc pas par l’arrété du 23 janvier 97. Ce texte n’est applicable que pr les installation modifié et mis en service apres juillet 1997
- Arrêté du 20 août 1985 relatif aux bruits aériens émis dans l'environnement par les installations classées pour la protection de l'environnement (pour les installations classées avant la mise en service ou modification avant le 1er juillet 1997)
- Arrêté du 15 novembre 1999 modifiant l'arrêté du 23 janvier 1997 relatif à la limitation des bruits émis dans l'environnement par les installations classées pour la protection de l'environnement et l'arrêté du 14 mai 1993 relatif à l'industrie du verre. 
- Arrêté du 12 mars 2003 relatif à l'industrie du verre et de la fibre minérale (articles 56, 57 et 58)
- Arrêté du 22 septembre 1994 relatif aux exploitations de carrières ;
- Arrêté du 24 janvier 2001 modifiant l'arrêté du 22 septembre 1994 relatif aux exploitations de carrières et aux installations de premier traitement des matériaux de carrières et l'arrêté du 23 janvier 1997 relatif à la limitation des bruits émis dans l'environnement par les installations classées pour la protection de l'environnement 
- Arrêté du 3 mai 1993 relatifs aux cimenteries 
- Arrêté du 3 avril 2000 relatif à l'industrie papetière 
- Arrêté du 25 janvier 1991 relatif aux installations d’incinération de résidus urbains (attention ce n’est pas  du tt  mm periodes que janvier 1997)
- Arrêté du 26 août 2011 relatif aux installations de production d'électricité utilisant l'énergie mécanique du vent au sein d'une installation soumise à autorisation au titre de la rubrique 2980 de la législation des installations classées pour la protection de l'environnement ;
- Circulaire du 29 août 2011 relative aux conséquences et orientations du classement des éoliennes dans le régime des installations classées. 

Bruit des activités artisanales, commerciales et industrielles non classées :
Les bruits des activités artisanales, commerciales et industrielles non classées sont régis par le décret du 31 août 2006 (évolution du décret n° 95-408 du 18 avril 1995), des dispositions réglementaires relatives à la lutte contre les bruits des activités ont été introduites dans le Code de la santé publique. 
### ***Bruit ferroviaire***
Le bruit des infrastructures ferroviaires, nouvelles ou faisant l’objet de modifications, est réglementé par 
- l’article L571-9 du Code de l’environnement, 
- le décret n° 95-22 du 9 janvier 1995  relatif à la limitation du bruit des aménagements et infrastructures de transports terrestres
- et l’arrêté du 8 novembre 1999 relatif au bruit des infrastructures ferroviaires