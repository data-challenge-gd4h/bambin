/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*
 * CREATION DU JEU DE DONNEES TEST
 * martin Schoreisz martin.schoreisz@cerema.fr
 * 
 */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/

CREATE EXTENSION tsm_system_rows;

/* ========================
 * 1. création de la météo
 ===========================*/ 

/* ------------------------------------------------------------------------------
 * 1.1 Table météo
 ------------------------------------------------------------------------------- */ 

-- CRéation de la table propagation.meteo 
INSERT INTO propagation.meteo(is_conventionnelle, is_quantitatif, is_station)
SELECT TRUE is_conventionnelle, (round(random())::int)::boolean is_quantitatif, (round(random())::int)::boolean is_station
 FROM generate_series(1, 65)
UNION ALL
SELECT FALSE is_conventionnelle, (round(random())::int)::boolean is_quantitatif, (round(random())::int)::boolean is_station
 FROM generate_series(1, 5) ;
 
/* ------------------------------------------------------------------------------
 * 1.2 Table des stations 
 ------------------------------------------------------------------------------- */ 
 
 -- création de la table des stations 
INSERT INTO propagation.station_meteo(is_station_fixe, description, geom)
SELECT TRUE is_station_fixe, 'Station Aéroport Bordeaux Mérignac'::TEXT description, st_setsrid((st_dump(ST_GeneratePoints(geom, 1))).geom, 2154) geom
 FROM temporaire."geom_BdxMet"
UNION
SELECT FALSE is_station_fixe, 'Station mobile BET '||to_char(random()*10+1, 'FM99') description, st_setsrid((st_dump(ST_GeneratePoints(g.geom, 1))).geom, 2154) geom
 FROM generate_series(1, 3), temporaire."geom_BdxMet" g ;
 
 /* ------------------------------------------------------------------------------
 * 1.3 Table des mesures
 ------------------------------------------------------------------------------- */ 
-- pour l'exemple, on va générer bcp de table avec des variation sur le pas temporel et la hauteur unique ou multiple
-- 3 pas temporel (15 min, 6 min, 60 min) pour une hauteur unique
-- 1 pas temporel (10 min) pour deux hauteur à 1 et 3 metres
-- ATTENTION, pour l'instant date_heure_fin est choisie comme date_heure_debut + 6 ou 10 ou 15 ou 60 min selon cas
INSERT INTO propagation.mesure_station_meteo(id_meteo, id_indicateur_meteo, date_heure_debut, date_heure_fin, id_station_meteo, valeur, hauteur)
WITH 
isole_mod_station AS (
SELECT *, ROW_NUMBER() OVER() id FROM propagation.meteo WHERE is_station = TRUE
),
_15min AS (
SELECT t.id_meteo
FROM isole_mod_station t
WHERE t.id <= 20
),
_6min AS ( 
SELECT t.id_meteo
FROM isole_mod_station t
WHERE t.id between 21 AND 25
),
_60min AS (
SELECT t.id_meteo
FROM isole_mod_station t
WHERE t.id between 26 AND 30
),
hauteur_multiple AS (
SELECT t.id_meteo
FROM isole_mod_station t
WHERE t.id > 30
),
-- Pas temporel de 15 minutes (hauteur fixe)
date_start_15min as(
SELECT t.id_meteo, current_date - (random() * (365 * random()*6))::int date_deb
 FROM _15min t),
date_fin_15min as(
 SELECT *, (date_deb + '1 day'::INTERVAL)::date date_fin_15min  FROM date_start_15min),
decomp_temps_15min_sansfin AS (
SELECT id_meteo, generate_series(date_deb::timestamptz, date_fin_15min::timestamptz, '15 min'::interval) date_heure_debut
 FROM date_fin_15min m ),
decomp_temps_15min AS (
SELECT *, (date_heure_debut::timestamptz + '15 min'::INTERVAL) date_heure_fin
 FROM decomp_temps_15min_sansfin),
indics_15min AS ( 
SELECT id_meteo, UNNEST(array_agg(DISTINCT trunc(random() * b + 1))) indics
 FROM generate_series((SELECT min(id_meteo) FROM date_fin_15min), (SELECT max(id_meteo) FROM date_fin_15min)) id_meteo, generate_series(1, 6) b
 GROUP BY id_meteo
),
station_15min AS (
SELECT id_meteo, trunc(random() * 3 + 1) id_station
 FROM date_fin_15min),
fin_15min AS (
SELECT *, CASE WHEN indics = 1 THEN random() * 50 -10 
               WHEN indics = 2 THEN random() * 20 + 1
               WHEN indics = 3 THEN random() * 360 + 0 
               WHEN indics = 4 THEN random() * 800 + 0
               WHEN indics = 5 THEN random() * 10 + 0
               WHEN indics = 6 THEN random() * 100 + 0 END valeur,
       round(random() * 10 + 1) hauteur
 FROM indics_15min i JOIN decomp_temps_15min using(id_meteo)
               JOIN station_15min USING (id_meteo)),
-- Pas temporel de 6 minutes (hauteur fixe)
date_start_6min as(
SELECT t.id_meteo, current_date - (random() * (365 * random()*6))::int date_deb
 FROM _6min t),
date_fin_6min as(
 SELECT *, (date_deb + '1 day'::INTERVAL)::date date_fin_6min  FROM date_start_6min),
decomp_temps_6min_sansfin AS (
SELECT id_meteo, generate_series(date_deb::timestamptz, date_fin_6min::timestamptz, '6 min'::interval) date_heure_debut
 FROM date_fin_6min m ),
decomp_temps_6min AS (
SELECT *, (date_heure_debut::timestamptz + '6 min'::INTERVAL) date_heure_fin
 FROM decomp_temps_6min_sansfin),
indics_6min AS ( 
SELECT id_meteo, UNNEST(array_agg(DISTINCT trunc(random() * b + 1))) indics
 FROM generate_series((SELECT min(id_meteo) FROM date_fin_6min), (SELECT max(id_meteo) FROM date_fin_6min)) id_meteo, generate_series(1, 6) b
 GROUP BY id_meteo
),
station_6min AS (
SELECT id_meteo, trunc(random() * 3 + 1) id_station
 FROM date_fin_6min),
fin_6min AS (
SELECT *, CASE WHEN indics = 1 THEN random() * 50 -10 
               WHEN indics = 2 THEN random() * 20 + 1
               WHEN indics = 3 THEN random() * 360 + 0 
               WHEN indics = 4 THEN random() * 800 + 0
               WHEN indics = 5 THEN random() * 10 + 0
               WHEN indics = 6 THEN random() * 100 + 0 END valeur,
       round(random() * 10 + 1) hauteur
 FROM indics_6min i JOIN decomp_temps_6min using(id_meteo)
               JOIN station_6min USING (id_meteo)),
-- Pas temporel de 60 minutes (hauteur fixe)
date_start_60min as(
SELECT t.id_meteo, current_date - (random() * (365 * random()*6))::int date_deb
 FROM _60min t),
date_fin_60min as(
 SELECT *, (date_deb + '1 day'::INTERVAL)::date date_fin_60min  FROM date_start_60min),
decomp_temps_60min_sansfin AS (
SELECT id_meteo, generate_series(date_deb::timestamptz, date_fin_60min::timestamptz, '60 min'::interval) date_heure_debut
 FROM date_fin_60min m ),
decomp_temps_60min AS (
SELECT *, (date_heure_debut::timestamptz + '60 min'::INTERVAL) date_heure_fin
 FROM decomp_temps_60min_sansfin),
indics_60min AS ( 
SELECT id_meteo, UNNEST(array_agg(DISTINCT trunc(random() * b + 1))) indics
 FROM generate_series((SELECT min(id_meteo) FROM date_fin_60min), (SELECT max(id_meteo) FROM date_fin_60min)) id_meteo, generate_series(1, 6) b
 GROUP BY id_meteo
),
station_60min AS (
SELECT id_meteo, trunc(random() * 3 + 1) id_station
 FROM date_fin_60min),
fin_60min AS (
SELECT *, CASE WHEN indics = 1 THEN random() * 50 -10 
               WHEN indics = 2 THEN random() * 20 + 1
               WHEN indics = 3 THEN random() * 360 + 0 
               WHEN indics = 4 THEN random() * 800 + 0
               WHEN indics = 5 THEN random() * 10 + 0
               WHEN indics = 6 THEN random() * 100 + 0 END valeur,
       round(random() * 10 + 1) hauteur
 FROM indics_60min i JOIN decomp_temps_60min using(id_meteo)
               JOIN station_60min USING (id_meteo)),
-- Hauteur multiple (pas temporel fixe)
date_start_hauteur_multiple as(
SELECT t.id_meteo, current_date - (random() * (365 * random()*6))::int date_deb
 FROM hauteur_multiple t),
date_fin_hauteur_multiple as(
 SELECT *, (date_deb + '1 day'::INTERVAL)::date date_fin_hauteur_multiple  FROM date_start_hauteur_multiple),
decomp_temps_hauteur_multiple_sansfin AS (
SELECT id_meteo, generate_series(date_deb::timestamptz, date_fin_hauteur_multiple::timestamptz, '10 min'::interval) date_heure_debut
 FROM date_fin_hauteur_multiple m ),
decomp_temps_hauteur_multiple AS (
SELECT *, (date_heure_debut::timestamptz + '10 min'::INTERVAL) date_heure_fin
 FROM decomp_temps_hauteur_multiple_sansfin),
indics_hauteur_multiple AS ( 
SELECT id_meteo, UNNEST(array_agg(DISTINCT trunc(random() * b + 1))) indics
 FROM generate_series((SELECT min(id_meteo) FROM date_fin_hauteur_multiple), (SELECT max(id_meteo) FROM date_fin_hauteur_multiple)) id_meteo, generate_series(1, 6) b
 GROUP BY id_meteo
),
station_hauteur_multiple AS (
SELECT id_meteo, trunc(random() * 3 + 1) id_station
 FROM date_fin_hauteur_multiple),
fin_hauteur_multiple AS (
SELECT *, CASE WHEN indics = 1 THEN random() * 50 -10 
               WHEN indics = 2 THEN random() * 20 + 1
               WHEN indics = 3 THEN random() * 360 + 0 
               WHEN indics = 4 THEN random() * 800 + 0
               WHEN indics = 5 THEN random() * 10 + 0
               WHEN indics = 6 THEN random() * 100 + 0 END valeur,
       unnest(ARRAY[1,3]) hauteur
 FROM indics_hauteur_multiple i JOIN decomp_temps_hauteur_multiple using(id_meteo)
               JOIN station_hauteur_multiple USING (id_meteo))
-- On remplit
SELECT * FROM fin_15min UNION SELECT * FROM fin_6min UNION SELECT * FROM fin_60min UNION SELECT * FROM fin_hauteur_multiple ORDER BY id_meteo, date_heure_debut, indics ;

 /* ------------------------------------------------------------------------------
 * 1.4 Table des observations
 ------------------------------------------------------------------------------- */

-- observation meteo
-- ATTENTION, pour l'instant date_heure_fin est choisie comme date_heure_debut + 480min
INSERT INTO propagation.observation_meteo(id_meteo, date_heure_debut, date_heure_fin, valeur)
WITH 
isole_mod_station AS (
SELECT *, ROW_NUMBER() OVER() id FROM propagation.meteo WHERE is_station = FALSE
),
date_start_obs as(
SELECT t.id_meteo, current_date - (random() * (365 * random()*6))::int date_deb
 FROM isole_mod_station t),
date_fin_obs as(
 SELECT *, (date_deb + '1 day'::INTERVAL)::date date_fin_obs  FROM date_start_obs),
decomp_temps_obs_sansfin AS (
SELECT id_meteo, generate_series(date_deb::timestamptz, date_fin_obs::timestamptz, '480 min'::interval) date_heure_debut
 FROM date_fin_obs m ),
decomp_temps_obs AS (
SELECT *, (date_heure_debut::timestamptz + '480 min'::INTERVAL) date_heure_fin
 FROM decomp_temps_obs_sansfin),
indics_obs AS ( 
SELECT id_meteo
 FROM generate_series((SELECT min(id_meteo) FROM date_fin_obs), (SELECT max(id_meteo) FROM date_fin_obs)) id_meteo
)
SELECT *, UNNEST(ARRAY['U'||trunc(random() * 5 + 1), 'T'||trunc(random() * 5 + 1)]) indics
 FROM decomp_temps_obs JOIN indics_obs using(id_meteo)ORDER BY id_meteo, date_heure_debut ;



/* =============================================================================
 * 2. Création des mesures (localisation, materiel, mesure, resultat, frequence)
 ============================================================================== */


/* ------------------------------------------------------------------------------
 * 2.1 création des matériels
 ------------------------------------------------------------------------------- */ 
 
 -- creation d'une liste de matériels de référence
 -- on va diversifier les marques, et proposé également des cas sans marque et numéro de série et des cas avec que la marque
 INSERT INTO mesure_bruit.materiel_mesure(id_type_materiel, id_marque_materiel, numero_serie)
 SELECT trunc(random() * 2 + 1) id_type_materiel, trunc(random() * 2 + 1) id_marque_materiel , substr(md5(random()::text), 0, trunc(random()*20 + 1)::integer) numero_serie
  FROM generate_series(1,4)
UNION
  SELECT 3 id_type_materiel, NULL id_marque_materiel , NULL numero_serie
 UNION
 SELECT trunc(random() * 2 + 1) id_type_materiel, trunc(random() * 2 + 1) id_marque_materiel , NULL numero_serie
  FROM generate_series(1,2) ;
 
 /* ------------------------------------------------------------------------------
 * 2.2 Création des géométries
 ------------------------------------------------------------------------------- */ 

-- issu de la géométrie de Bordeaux Métropole 
INSERT INTO mesure_bruit.localisation_mesure(geom)
SELECT st_setsrid((st_dump(ST_GeneratePoints(geom, 100))).geom, 2154)
 FROM temporaire."geom_BdxMet" ;

 /* ------------------------------------------------------------------------------
 * 2.3 Création des mesures
 ------------------------------------------------------------------------------- */ 
 
-- il y a potentiellement bcp de cas, on va se concentrer ici sur les suivants : 
-- séparation des points fixe et des pélevements
-- - MOa connu et MOe connu, MOa connu et MOe inconnu
-- localisation de mesure partagée ou non - attention à la temporalité
-- au moins une mesure en intérieur, avec descriptif de la piece et de l'orientation
-- qq mesures en champs libre, d'autres en façade
-- au moin 1 mesure en continu 
-- si météo connu, date selon météo
-- duree d'integratio de 0.125 ou 1
-- meteo présente ou non

-- Cas des points fixes
INSERT INTO mesure_bruit.mesure_bruit (maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, 
    is_exterieur, is_facade, is_champ, hauteur_mesure, id_piece_mesure, id_piece_mesure_orient, is_ponctuel, date_debut, date_fin, 
    duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo)
-- localisation partagée
WITH 
date_meteo AS (
 SELECT id_meteo, min(date_heure_debut) date_debut, max(date_heure_fin) date_fin
  FROM propagation.meteo m JOIN propagation.mesure_station_meteo msm USING(id_meteo)
  GROUP BY id_meteo
 UNION ALL
 SELECT id_meteo, min(date_heure_debut) date_debut, max(date_heure_fin) date_fin
  FROM propagation.meteo m JOIN propagation.observation_meteo om USING(id_meteo)
  GROUP BY id_meteo),
-- meteo connue
loc_part_meteo_connue AS (
 SELECT (array['13000171200368', '24330031600011', '13001045700013', '48760720200024', '41228073700476'])[trunc(random() * 5 + 1)] maitre_ouvrage, 
       (array['35289994200051','35289994200051','44421420900025','40150266100010','52757314100043','13001831000016'])[trunc(random() * 5 + 1)]maitre_oeuvre, 
       TRUE is_point_fixe, 
       m.id_localisation_mesure, TRUE is_exterieur, TRUE is_facade, NULL::boolean is_champ, trunc(random() * 6 + 1) hauteur_mesure, 
       NULL::int id_piece_mesure, NULL::int id_piece_mesure_orient, (round(random())::int)::boolean is_ponctuel, m2.date_debut, m2.date_fin,
       (array[0.125, 1::float])[trunc(random() * 2 + 1)] duree_integration, 
       trunc(random() * 6 + 1) id_materiel_mesure, NULL::int id_rapport, 
       NULL::int id_campagne_mesure, m2.id_meteo
  FROM mesure_bruit.localisation_mesure m JOIN date_meteo m2 ON m.id_localisation_mesure = m2.id_meteo - 200
  WHERE id_localisation_mesure <= 10
 UNION ALL
 SELECT (array['13000171200368', '24330031600011', '13001045700013', '48760720200024', '41228073700476'])[trunc(random() * 5 + 1)] maitre_ouvrage, 
       (array['35289994200051','35289994200051','44421420900025','40150266100010','52757314100043','13001831000016'])[trunc(random() * 5 + 1)]maitre_oeuvre, 
       TRUE is_point_fixe, 
       m.id_localisation_mesure, TRUE is_exterieur, TRUE is_facade, NULL::boolean is_champ, trunc(random() * 6 + 1) hauteur_mesure, 
       NULL::int id_piece_mesure, NULL::int id_piece_mesure_orient, (round(random())::int)::boolean is_ponctuel, m2.date_debut, m2.date_fin,
       (array[0.125, 1::float])[trunc(random() * 2 + 1)] duree_integration, 
       trunc(random() * 6 + 1) id_materiel_mesure, NULL::int id_rapport, 
       NULL::int id_campagne_mesure, m2.id_meteo
  FROM mesure_bruit.localisation_mesure m JOIN date_meteo m2 ON m.id_localisation_mesure = m2.id_meteo - 210
  WHERE id_localisation_mesure <= 10),
--meteo inconnue exterieur champ libre
loc_part_meteo_inconnue_ext_date_deb1 AS (
SELECT (array['13000171200368', '24330031600011', '13001045700013', '48760720200024', '41228073700476'])[trunc(random() * 5 + 1)] maitre_ouvrage, 
       (array['35289994200051','35289994200051','44421420900025','40150266100010','52757314100043','13001831000016'])[trunc(random() * 5 + 1)]maitre_oeuvre, 
       TRUE is_point_fixe, 
       m.id_localisation_mesure, TRUE is_exterieur, FALSE is_facade, NULL::boolean is_champ, trunc(random() * 6 + 1) hauteur_mesure, 
       NULL::int id_piece_mesure, NULL::int id_piece_mesure_orient, (round(random())::int)::boolean is_ponctuel, 
       current_date - (random() * (365 * (random()*20) + (365 *10)))::int date_debut, 
       (array[0.125, 1::float])[trunc(random() * 2 + 1)] duree_integration, 
       trunc(random() * 6 + 1) id_materiel_mesure, NULL::int id_rapport, 
       NULL::int id_campagne_mesure, NULL id_meteo
  FROM mesure_bruit.localisation_mesure m
  WHERE id_localisation_mesure BETWEEN 11 AND 20),
loc_part_meteo_inconnue_ext1 as(
SELECT maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, is_exterieur, is_facade, is_champ, hauteur_mesure, id_piece_mesure::int, id_piece_mesure_orient::int, is_ponctuel, date_debut::timestamptz, 
       (date_debut+(to_char(trunc(random()*2+1), 'FM9')||' day')::INTERVAL)::timestamptz date_fin, 
       duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo::int
 FROM loc_part_meteo_inconnue_ext_date_deb1),
loc_part_meteo_inconnue_ext_date_deb2 AS (
SELECT (array['13000171200368', '24330031600011', '13001045700013', '48760720200024', '41228073700476'])[trunc(random() * 5 + 1)] maitre_ouvrage, 
       (array['35289994200051','35289994200051','44421420900025','40150266100010','52757314100043','13001831000016'])[trunc(random() * 5 + 1)]maitre_oeuvre, 
       TRUE is_point_fixe, 
       m.id_localisation_mesure, TRUE is_exterieur, FALSE is_facade, NULL::boolean is_champ, trunc(random() * 6 + 1) hauteur_mesure, 
       NULL id_piece_mesure, NULL id_piece_mesure_orient, (round(random())::int)::boolean is_ponctuel, 
       current_date - (random() * (365 * (random()*2) + (365)))::int date_debut, 
       (array[0.125, 1::float])[trunc(random() * 2 + 1)] duree_integration, 
       trunc(random() * 6 + 1) id_materiel_mesure, NULL::int id_rapport, 
       NULL::int id_campagne_mesure, NULL id_meteo
  FROM mesure_bruit.localisation_mesure m
  WHERE id_localisation_mesure BETWEEN 11 AND 20),
loc_part_meteo_inconnue_ext2 as(
SELECT maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, is_exterieur, is_facade, is_champ, hauteur_mesure, 
       id_piece_mesure::int, id_piece_mesure_orient::int, is_ponctuel, date_debut::timestamptz, 
       (date_debut+(to_char(trunc(random()*2+1), 'FM9')||' day')::INTERVAL)::timestamptz date_fin, 
       duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo::int
 FROM loc_part_meteo_inconnue_ext_date_deb2),
loc_part_meteo_inconnue_ext AS (
SELECT * FROM loc_part_meteo_inconnue_ext1 UNION ALL SELECT * FROM loc_part_meteo_inconnue_ext2),
--meteo inconnue interieur champ libre
loc_part_meteo_inconnue_int_date_deb1 AS (
SELECT (array['13000171200368', '24330031600011', '13001045700013', '48760720200024', '41228073700476'])[trunc(random() * 5 + 1)] maitre_ouvrage, 
       (array['35289994200051','35289994200051','44421420900025','40150266100010','52757314100043','13001831000016'])[trunc(random() * 5 + 1)]maitre_oeuvre, 
       TRUE is_point_fixe, 
       m.id_localisation_mesure, FALSE is_exterieur, NULL::boolean is_facade, NULL::boolean is_champ, trunc(random() * 6 + 1) hauteur_mesure, 
       trunc(random() * 6 + 1) id_piece_mesure, trunc(random() * 9 + 1) id_piece_mesure_orient, (round(random())::int)::boolean is_ponctuel, 
       current_date - (random() * (365 * (random()*20) + (365 *10)))::int date_debut, 
       (array[0.125, 1::float])[trunc(random() * 2 + 1)] duree_integration, 
       trunc(random() * 6 + 1) id_materiel_mesure, NULL::int id_rapport, 
       NULL::int id_campagne_mesure, NULL id_meteo
  FROM mesure_bruit.localisation_mesure m JOIN date_meteo m2 ON m.id_localisation_mesure = m2.id_meteo - 210
  WHERE id_localisation_mesure BETWEEN 21 AND 25),
loc_part_meteo_inconnue_int1 as( 
SELECT maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, is_exterieur, is_facade, is_champ, hauteur_mesure, id_piece_mesure, id_piece_mesure_orient, is_ponctuel, date_debut::timestamptz, 
       (date_debut+(to_char(trunc(random()*2+1), 'FM9')||' day')::INTERVAL)::timestamptz date_fin, 
       duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo::int
 FROM loc_part_meteo_inconnue_int_date_deb1),
loc_part_meteo_inconnue_int_date_deb2 AS (
SELECT maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, is_exterieur, is_facade, is_champ,
       hauteur_mesure, id_piece_mesure, id_piece_mesure_orient, is_ponctuel, 
       (current_date - (random() * (365 * (random()*2) + (365)))::int)::timestamptz date_debut, 
       duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo
 FROM loc_part_meteo_inconnue_int1),
loc_part_meteo_inconnue_int2 as( 
SELECT maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, is_exterieur, is_facade, is_champ, 
       hauteur_mesure, id_piece_mesure, id_piece_mesure_orient, is_ponctuel, date_debut::timestamptz, 
       (date_debut+(to_char(trunc(random()*2+1), 'FM9')||' day')::INTERVAL)::timestamptz date_fin, 
       duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo::int
 FROM loc_part_meteo_inconnue_int_date_deb2),
loc_part_meteo_inconnue as(
SELECT * FROM loc_part_meteo_inconnue_int1 UNION ALL SELECT * FROM loc_part_meteo_inconnue_int2 UNION ALL
SELECT * FROM loc_part_meteo_inconnue_ext1 UNION ALL SELECT * FROM loc_part_meteo_inconnue_ext2),
localisation_partagee AS (
SELECT * FROM loc_part_meteo_connue UNION ALL SELECT * FROM loc_part_meteo_inconnue),
--localisation unique
-- meteo connue
loc_uniq_meteo_connue AS (
SELECT (array['13000171200368', '24330031600011', '13001045700013', '48760720200024', '41228073700476'])[trunc(random() * 5 + 1)] maitre_ouvrage, 
       (array['35289994200051','35289994200051','44421420900025','40150266100010','52757314100043','13001831000016'])[trunc(random() * 5 + 1)]maitre_oeuvre, 
       TRUE is_point_fixe, 
       m.id_localisation_mesure, TRUE is_exterieur, TRUE is_facade, NULL::boolean is_champ, trunc(random() * 6 + 1) hauteur_mesure, 
       NULL::int id_piece_mesure, NULL::int id_piece_mesure_orient, (round(random())::int)::boolean is_ponctuel, m2.date_debut, m2.date_fin,
       (array[0.125, 1::float])[trunc(random() * 2 + 1)] duree_integration, 
       trunc(random() * 6 + 1) id_materiel_mesure, NULL::int id_rapport, 
       NULL::int id_campagne_mesure, m2.id_meteo
  FROM mesure_bruit.localisation_mesure m JOIN date_meteo m2 ON m.id_localisation_mesure = m2.id_meteo - 195
  WHERE id_localisation_mesure BETWEEN 26 AND 75),
-- meteo inconnue
loc_uniq_meteo_inconnue_date_deb AS (
SELECT (array['13000171200368', '24330031600011', '13001045700013', '48760720200024', '41228073700476'])[trunc(random() * 5 + 1)] maitre_ouvrage, 
       (array['35289994200051','35289994200051','44421420900025','40150266100010','52757314100043','13001831000016'])[trunc(random() * 5 + 1)]maitre_oeuvre, 
       TRUE is_point_fixe, 
       m.id_localisation_mesure, TRUE is_exterieur, FALSE is_facade, NULL::boolean is_champ, trunc(random() * 6 + 1) hauteur_mesure, 
       NULL id_piece_mesure, NULL id_piece_mesure_orient, (round(random())::int)::boolean is_ponctuel, 
       current_date - (random() * (365 * (random()*2) + (365)))::int date_debut, 
       (array[0.125, 1::float])[trunc(random() * 2 + 1)] duree_integration, 
       trunc(random() * 6 + 1) id_materiel_mesure, NULL::int id_rapport, 
       NULL::int id_campagne_mesure, NULL id_meteo
  FROM mesure_bruit.localisation_mesure m
  WHERE id_localisation_mesure BETWEEN 76 AND 95),
loc_uniq_meteo_inconnue as(
SELECT maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, is_exterieur, is_facade, is_champ,
       hauteur_mesure, id_piece_mesure::int, id_piece_mesure_orient::int, is_ponctuel, date_debut::timestamptz, 
       (date_debut+(to_char(trunc(random()*7+1), 'FM9')||' day')::INTERVAL)::timestamptz date_fin, 
       duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo::int
 FROM loc_uniq_meteo_inconnue_date_deb),
localisation_uniq AS (
SELECT * FROM loc_uniq_meteo_connue UNION ALL SELECT * FROM loc_uniq_meteo_inconnue)
SELECT * FROM localisation_uniq UNION ALL SELECT * FROM localisation_partagee ; 

--cas des prélèvement (en localisation uniq, météo inconnue)
INSERT INTO mesure_bruit.mesure_bruit (maitre_ouvrage, maitre_oeuvre, is_point_fixe, id_localisation_mesure, 
    is_exterieur, is_facade, is_champ, hauteur_mesure, id_piece_mesure, id_piece_mesure_orient, is_ponctuel, date_debut, date_fin, 
    duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo)
SELECT maitre_ouvrage, maitre_oeuvre, FALSE is_point_fixe, id_localisation_mesure, is_exterieur, is_facade, is_champ,
       hauteur_mesure, id_piece_mesure::int, id_piece_mesure_orient::int, is_ponctuel, 
       (date_debut+(to_char(trunc(random()*2+2), 'FM9')||' hours')::INTERVAL)::timestamptz date_debut,
       (date_debut+(to_char(trunc(random()*5+3), 'FM9')||' hours')::INTERVAL)::timestamptz date_fin, 
       duree_integration, id_materiel_mesure, id_rapport, id_campagne_mesure, id_meteo::int
 FROM mesure_bruit.mesure_bruit
 WHERE id_mesure_bruit BETWEEN 194 AND 198 ; 


/* ===========================================================
 * 3. Création des sources de bruit (aérien, ICPE, fer, route)
 =============================================================*/

/* --------------------
 * 3.1 sources de bruit
 ---------------------*/
 
INSERT INTO emission.source_de_bruit (type_source_bruit)
SELECT 3 type_source_bruit
 FROM generate_series(1,10)
UNION ALL
SELECT 4 type_source_bruit
 FROM generate_series(1,20)
UNION ALL
SELECT 1 type_source_bruit
 FROM generate_series(1,30)
UNION ALL
SELECT 2 type_source_bruit
 FROM generate_series(1,20) ;
 
/* --------------------
 * 3.1 Affection des sources aux mesures
 ---------------------*/

INSERT INTO mesure_bruit.relation_source_mesure (id_mesure_bruit, id_source_de_bruit)
--plusieurs sources pour unemesure
WITH 
premiere_affectation AS (
SELECT UNNEST((SELECT array_agg(id_mesure_bruit ORDER BY random()) FROM mesure_bruit.mesure_bruit WHERE  is_point_fixe = TRUE)) id_mesure_bruit, 
       UNNEST((SELECT array_agg(id_source_de_bruit ORDER BY random()) FROM emission.source_de_bruit)) id_source_de_bruit),
affectation_mesure_doublement_affectes as(
SELECT UNNEST((SELECT array_agg(id_mesure_bruit) FROM premiere_affectation WHERE id_source_de_bruit IS NULL)) id_mesure_bruit, 
       UNNEST((SELECT array_agg(id_source_de_bruit ORDER BY random()) FROM emission.source_de_bruit)) id_source_de_bruit),
mesure_multi_source AS (
SELECT * FROM affectation_mesure_doublement_affectes WHERE id_mesure_bruit IS NOT NULL 
UNION ALL 
SELECT * FROM premiere_affectation WHERE id_source_de_bruit IS NOT NULL),
--plusieurs mesures pour une source
--source à une seule mesure 
source_mono_mesure AS (
SELECT id_source_de_bruit, count(*)
 FROM mesure_multi_source
 GROUP BY id_source_de_bruit
 HAVING count(*) = 1),
source_multi_mesure as(
SELECT unnest((SELECT array_agg(id_mesure_bruit ORDER BY random()) FROM mesure_bruit.mesure_bruit)) id_mesure_bruit,
	   UNNEST((SELECT array_agg(id_source_de_bruit ORDER BY random()) FROM source_mono_mesure)) id_source_de_bruit  
 LIMIT 10)
-- au final des cas avec une source plusieurs mesure, unemesure plusieurs sources, ou une source une mesure
SELECT id_mesure_bruit, id_source_de_bruit 
 FROM mesure_multi_source 
 where id_source_de_bruit is not null and id_mesure_bruit is not null 
UNION ALL 
SELECT id_mesure_bruit, id_source_de_bruit 
 FROM source_multi_mesure 
 where id_source_de_bruit is not null and id_mesure_bruit is not null;

/*----------
 * 3.2 ICPE
 ----------*/
 
--CREATE EXTENSION tsm_system_rows ; 
 
--création de l'émission ICPE

CREATE TEMP TABLE enum_numeros_siret (
  "numero_siret" varchar NOT NULL,
  "denomination" varchar,
  PRIMARY KEY ("numero_siret")
) ;
COPY enum_numeros_siret(denomination, numero_siret) FROM 'datas_perso\siret_exemple_33.csv' CSV HEADER DELIMITER ',' ;

INSERT INTO emission.icpe (numero_siret, id_source_de_bruit, description_icpe, observation_duree_bruit_particulier, observation_moment_manifestation, observation_conditions_emission_bruit, date_heure_debut_obs, date_heure_fin_obs)
WITH 
concatenation AS (
SELECT UNNEST(array_agg(DISTINCT e.id_source_de_bruit)) id_source_de_bruit, UNNEST(array_agg(DISTINCT s.numero_siret)) numero_siret
 FROM emission.source_de_bruit e, (SELECT * FROM enum_numeros_siret TABLESAMPLE SYSTEM_ROWS(9)) s
 WHERE e.type_source_bruit = 3),
duplicata AS (
SELECT id_source_de_bruit, 
 CASE WHEN numero_siret IS NULL THEN (SELECT (array_agg(numero_siret))[trunc(random()*9+1)] FROM concatenation) ELSE numero_siret END numero_siret,
 substr(md5(random()::text), 0, trunc(random()*25 + 1)::integer) description_icpe,
 substr(md5(random()::text), 0, trunc(random()*50 + 1)::integer) observation_duree_bruit_particulier,
 substr(md5(random()::text), 0, trunc(random()*50 + 1)::integer) observation_moment_manifestation,
 substr(md5(random()::text), 0, trunc(random()*50 + 1)::integer) observation_conditions_emission_bruit
 FROM concatenation),
duplicata_et_null AS (
SELECT numero_siret, id_source_de_bruit, description_icpe, 
       CASE WHEN id_source_de_bruit < 3 THEN NULL ELSE observation_duree_bruit_particulier END observation_duree_bruit_particulier, 
       CASE WHEN id_source_de_bruit < 3 THEN NULL ELSE observation_moment_manifestation END observation_moment_manifestation, 
       CASE WHEN id_source_de_bruit < 3 THEN NULL ELSE observation_conditions_emission_bruit END observation_conditions_emission_bruit 
       FROM duplicata),
donnees_finales AS (
SELECT den.numero_siret, 
       den.id_source_de_bruit, 
       den.description_icpe, 
       den.observation_duree_bruit_particulier, 
       den.observation_moment_manifestation, 
       den.observation_conditions_emission_bruit, 
       mb.date_debut date_heure_debut_obs, 
       mb.date_fin date_heure_fin_obs
FROM duplicata_et_null den
JOIN mesure_bruit.relation_source_mesure rsm ON den.id_source_de_bruit=rsm.id_source_de_bruit
JOIN mesure_bruit.mesure_bruit mb ON mb.id_mesure_bruit = rsm.id_mesure_bruit
)
SELECT * FROM donnees_finales ;

DROP TABLE enum_numeros_siret ;



/*---------------------
 * 3.2 sources aérienne
 ---------------------*/
 
 -- appareils aérien
INSERT INTO emission.appareil_aerien (identifiant_unique_international_avion, id_avion_modele, id_avion_famille, id_motorisation)
WITH
base AS (
SELECT 'F-'||substr(md5(random()::text), 0, 5) identifiant_unique_international_avion, ROW_NUMBER() over() id_base
 FROM generate_series(1, 60))
SELECT identifiant_unique_international_avion,
       trunc(random() * 2 +1) id_avion_modele,
       trunc(random() * 3 +1) id_avion_famille,
       NULL::int4 id_motorisation
 FROM base
 WHERE id_base < 16
UNION ALL 
SELECT identifiant_unique_international_avion,
       trunc(random() * 2 +1) id_avion_modele,
       NULL::int4 id_avion_famille,
       NULL::int4 id_motorisation
 FROM base
 WHERE id_base BETWEEN 16 AND 30
UNION ALL 
SELECT identifiant_unique_international_avion,
       NULL::int4 id_avion_modele,
       trunc(random() * 3 +1) id_avion_famille,
       NULL::int4 id_motorisation
 FROM base
 WHERE id_base BETWEEN 31 AND 40
UNION ALL 
SELECT identifiant_unique_international_avion,
       NULL::int4 id_avion_modele,
       NULL::int4 id_avion_famille,
       NULL::int4 id_motorisation
 FROM base
 WHERE id_base > 40 ;

--circulation aérienne
INSERT INTO emission.circulation_aerienne (id_source_de_bruit, identifiant_unique_international_avion, aeroport, date_heure_passage)
WITH 
avion_par_source AS (
SELECT sdb.id_source_de_bruit, mb.date_debut, mb.date_fin, generate_series(1, trunc(random() * 50 + 10)::int) s,
       ((SELECT array_agg(identifiant_unique_international_avion) FROM emission.appareil_aerien))[trunc(random() * 60 +1) ] identifiant_unique_international_avion,
       15 aeroport
 FROM emission.source_de_bruit sdb JOIN mesure_bruit.relation_source_mesure rsm using(id_source_de_bruit)
                                   JOIN mesure_bruit.mesure_bruit mb ON mb.id_mesure_bruit = rsm.id_mesure_bruit
 WHERE sdb.type_source_bruit = 4),
horaire_passage AS ( 
SELECT id_source_de_bruit, identifiant_unique_international_avion, aeroport,
       date_debut + (to_char(trunc(random()*23+1), 'FM99')||' hour')::INTERVAL
                  +  (to_char(trunc(random()*59+1), 'FM99')||' minutes')::INTERVAL
                  +  (to_char(trunc(random()*59+1), 'FM99')||' second')::INTERVAL date_heure_passage
 FROM avion_par_source
)
SELECT * FROM horaire_passage ;

-- trajectoire
-- fourniture d'un esuel exemple, base sur une ligne manuelle et des altitude forfaitaire
INSERT INTO emission.trajectoire (point_passage, date_heure_passage, id_circulation_aerienne)
SELECT (st_dumppoints(geom)).geom point_passage, 
       (SELECT date_heure_passage FROM emission.circulation_aerienne LIMIT 1) + (to_char(generate_series(1, 15), 'FM99')||' minute')::INTERVAL date_heure_passage,
       (SELECT id_circulation_aerienne FROM emission.circulation_aerienne LIMIT 1) id_circulation_aerienne
 FROM temporaire.trajectoire ;
 
 
/*------------------------
 * 3.3 sources ferroviaire
 ------------------------*/
 
 -- récupération de la table des voies ferrees
CREATE TEMP TABLE tmp_voie_ferree (
  "numero_ligne" varchar NOT NULL,
  "id_nature_ligne_fer" int4 NOT NULL,
  PRIMARY KEY ("numero_ligne")
) ;
COPY tmp_voie_ferree (numero_ligne, id_nature_ligne_fer) FROM 'datas_perso\ligne_fer.csv' CSV HEADER DELIMITER ',' ;
INSERT INTO emission.voie_ferree(numero_ligne, is_lgv)
SELECT numero_ligne, (id_nature_ligne_fer % 2)::boolean is_lgv FROM tmp_voie_ferree ;
DROP TABLE tmp_voie_ferree ;
UPDATE emission.voie_ferree SET nom_commun='Ligne 1000' where numero_ligne = '1000' ;

--création des infrastructures
INSERT INTO emission.infra_voie_ferree(numero_ligne, declivite, id_position_sol, id_type_pose_fer, id_point_singulier_fer, id_type_traverse, id_type_rail, id_rvt_fer)
WITH 
complet AS (
SELECT numero_ligne, 
       trunc(random() * (-6) + 3) declivite,
       trunc(random() * 3 + 1) id_position_sol,
       trunc(random() * 2 + 1) id_type_pose_fer,
       trunc(random() * 3 + 1) id_point_singulier_fer,
       trunc(random() * 3 + 1) id_rvt_traverse,
       trunc(random() * 2 + 1) id_type_rail,
       trunc(random() * 4 + 1) id_rvt_fer
 FROM emission.voie_ferree
 OFFSET 25),
partiel AS (
SELECT numero_ligne, 
       trunc(random() * (-8) + 5) declivite,
       trunc(random() * 3 + 1) id_position_sol,
       NULL::int id_type_pose_fer,
       trunc(random() * 3 + 1) id_point_singulier_fer,
       NULL::int id_rvt_traverse,
       trunc(random() * 2 + 1) id_type_rail,
       NULL::int id_rvt_fer
 FROM emission.voie_ferree
 limit 120),
vide AS (
SELECT numero_ligne,
       NULL::int declivite,
       NULL::int id_position_sol,
       NULL::int id_type_pose_fer,
       NULL::int id_point_singulier_fer,
       NULL::int id_rvt_traverse,
       NULL::int id_type_rail,
       NULL::int id_rvt_fer
  FROM emission.voie_ferree)
SELECT * FROM partiel UNION ALL SELECT * FROM complet UNION ALL SELECT * FROM vide ;

--creation des trafics
-- il faut gérer les cas de trafic_nature : constat, long terme
-- il faut gérer les indictauer TMJA ou débit
-- si debit, alors date heure, si tmja alors null pour date heure
-- il faut proposer au moins 1 exemple avec 2 sens différents
INSERT INTO emission.trafic_fer (id_infra_voie_ferree, sens_circulation, id_source_de_bruit, date_heure_debut, date_heure_fin, id_indicateur_trafic_fer, trafic_nature, vitesse, nb_passage)
WITH
infra AS (
SELECT array_agg(id_infra_voie_ferree) id_infra_voie_ferree
 FROM  (SELECT id_infra_voie_ferree 
 		FROM emission.infra_voie_ferree ivf 
 		ORDER BY random()
 		LIMIT (SELECT count(*) FROM emission.source_de_bruit WHERE type_source_bruit = 2)) t),
emission AS (
SELECT array_agg(sdb.id_source_de_bruit) id_source_de_bruit
 FROM emission.source_de_bruit sdb
 WHERE sdb.type_source_bruit = 2),
lien_infra_emission AS (
SELECT UNNEST(id_source_de_bruit) id_source_de_bruit, UNNEST(id_infra_voie_ferree)  id_infra_voie_ferree
 FROM infra, emission),
fer_par_source_constat AS (
SELECT UNNEST(array_agg(sdb.id_source_de_bruit)) id_source_de_bruit, 
       UNNEST(array_agg(sdb.id_infra_voie_ferree)) id_infra_voie_ferree,
       UNNEST(array_agg(mb.date_debut)) date_debut, UNNEST(array_agg(mb.date_fin)) date_fin, 
       1 trafic_nature, 
       UNNEST(CASE WHEN array_length(array_agg(sdb.id_source_de_bruit), 1)%2 = 0 THEN 
       		array_fill(1::integer, ARRAY[array_length(array_agg(sdb.id_source_de_bruit), 1)/2])|| 
       		          array_fill(2::integer, ARRAY[array_length(array_agg(sdb.id_source_de_bruit), 1)/2])
       		ELSE array_cat(array_fill(1::integer, ARRAY[array_length(array_agg(sdb.id_source_de_bruit), 1)/2]), 
       		          array_fill(2::integer, ARRAY[(array_length(array_agg(sdb.id_source_de_bruit), 1)/2)+1]))
       		END) id_indicateur_trafic_fer
 FROM lien_infra_emission sdb JOIN mesure_bruit.relation_source_mesure rsm using(id_source_de_bruit)
                                   JOIN mesure_bruit.mesure_bruit mb ON mb.id_mesure_bruit = rsm.id_mesure_bruit), 
-- gestion des sens : 1 source exemple avec du trafc par sens
constat_sens1_sens_uniques AS (
SELECT *, 13 sens_circulation
 FROM fer_par_source_constat
 LIMIT 1),
constat_sens2_sens_uniques AS (
SELECT *, 14 sens_circulation
 FROM fer_par_source_constat
 LIMIT 1),
constat_2sens AS (
SELECT *, 16 sens_circulation
 FROM fer_par_source_constat
 OFFSET 1),
constat_tout_sens AS (
SELECT * FROM constat_sens1_sens_uniques UNION ALL SELECT * FROM constat_sens2_sens_uniques UNION ALL SELECT * FROM constat_2sens),
-- si indic est TMJA, alors pas de date_heure_debut et date_heure_fin
tmj_constat AS (
SELECT id_infra_voie_ferree,
       sens_circulation,
       id_source_de_bruit,
       CASE WHEN id_indicateur_trafic_fer IN (1,3) THEN NULL 
            ELSE date_debut END date_heure_debut, 
       CASE WHEN id_indicateur_trafic_fer IN (1,3) THEN NULL 
            ELSE date_fin END date_heure_fin, 
       id_indicateur_trafic_fer,
       trafic_nature,
       trunc(random()*300+50)::integer vitesse, 
       (random()*100+1)::integer nb_passage
 FROM constat_tout_sens
 WHERE id_indicateur_trafic_fer = 1),
debits_horaires_constat AS (
-- gestion des débits horaires
 SELECT id_infra_voie_ferree,
       sens_circulation,
       id_source_de_bruit,
       generate_series(date_debut, (date_fin::timestamptz - '1 hour'::INTERVAL), '1 hour')::timestamptz date_heure_debut,   
       generate_series((date_debut::timestamptz + '1 hour'::INTERVAL), date_fin, '1 hour')::timestamptz date_heure_fin,
       id_indicateur_trafic_fer,
       trafic_nature,
       trunc(random()*300+50)::integer vitesse, 
       (random()*100+1)::integer nb_passage
  FROM constat_tout_sens
  WHERE id_indicateur_trafic_fer = 2),
-- exemple de long terme
long_terme AS (
SELECT id_infra_voie_ferree,
       sens_circulation,
       id_source_de_bruit,
       NULL::timestamptz date_heure_debut,
       NULL::timestamptz date_heure_fin,
       1 id_indicateur_trafic_fer,
       2 trafic_nature,
       trunc(random()*300+50)::integer vitesse, 
       (random()*100+1)::integer nb_passage
  FROM constat_tout_sens
  WHERE id_indicateur_trafic_fer = 2)
SELECT * FROM tmj_constat UNION ALL SELECT * FROM debits_horaires_constat UNION ALL SELECT * FROM long_terme;
-- Pourquoi cette ligne?
SELECT * FROM emission.infra_voie_ferree ivf TABLESAMPLE SYSTEM_ROWS((SELECT count(*) FROM emission.source_de_bruit WHERE type_source_bruit = 2)) ;

 
-- affectation des materiels roulants
INSERT INTO emission.materiel_roulant (id_trafic_fer, id_categorie_trafic_fer)
WITH
base AS (
SELECT t id_trafic_fer, generate_series(trunc(random() * 4 + 1)::integer, trunc(random() * 4 + 4)::integer, trunc(random() * 4 + 1)::integer) s
 FROM  generate_series(1,(SELECT max(id_trafic_fer) FROM emission.trafic_fer))t )
SELECT id_trafic_fer, UNNEST(array_agg(DISTINCT s)) id_categorie_trafic_fer
 FROM base
 GROUP BY id_trafic_fer ;

--composition des matériels roulant il y a des cas où on a pas la décomposition
INSERT INTO emission.composition_materiel_roulant (id_materiel_roulant, id_module_roulant, nombre)
WITH
base AS (
SELECT t id_materiel_roulant, generate_series(trunc(random() * 3 + 1)::integer, trunc(random() * 3 + 2)::integer, trunc(random() * 3 + 1)::integer) s
 FROM generate_series(1,(SELECT max(id_materiel_roulant) FROM emission.materiel_roulant))t)
SELECT id_materiel_roulant, UNNEST(array_agg(CASE WHEN s > 3 THEN trunc(random()*3+1)::int ELSE s end)) id_module_roulant, trunc(random()*15+1)::integer nombre
 FROM base
 GROUP BY id_materiel_roulant ;


/*------------------------
 * 3.4 sources routières
 ------------------------*/

--création des routes
INSERT INTO emission.route(id_nom_route, id_gest_route, id_dept, id_commune)
WITH 
base as(
SELECT trunc(random() * nb_route +1)::int id_nomroute, 
       trunc(random() * (SELECT max(id_gest_route) FROM emission.enum_gestionnaire) +1)::int id_gest_route,
       trunc(random() * (SELECT max(id_dept) FROM emission.enum_dept) +1)::int id_dept,
       trunc(random() * (SELECT max(id_commune) FROM emission.enum_commune) +1)::int id_commune
 FROM generate_series(1, (SELECT max(id_nom_route) nb_route FROM emission.enum_nom_route)) s,
 	LATERAL (SELECT max(id_nom_route) nb_route FROM emission.enum_nom_route) n),
nettoyage_commune AS (
 SELECT  id_nomroute,
         nom_route,
         id_gest_route,
         id_dept,
         CASE WHEN nom_route ~'^(A|D|N|M)[0-9].*' THEN NULL ELSE id_commune END id_commune  
  FROM base b JOIN emission.enum_nom_route r ON b.id_nomroute = r.id_nom_route),
unique_hors_commune AS (
SELECT DISTINCT ON (id_nomroute, id_gest_route , id_dept) id_nomroute, id_gest_route , id_dept, id_commune, nom_route
 FROM nettoyage_commune
 WHERE id_commune IS null
 ORDER BY id_nomroute, id_gest_route , id_dept),
unique_commune AS (
SELECT DISTINCT ON (id_nomroute, id_gest_route , id_dept, id_commune) id_nomroute, id_gest_route , id_dept, id_commune, nom_route
 FROM nettoyage_commune
 WHERE id_commune IS NOT null
 ORDER BY id_nomroute, id_gest_route , id_dept, id_commune)
SELECT id_nomroute, id_gest_route , id_dept, id_commune FROM unique_commune 
UNION ALL 
SELECT id_nomroute, id_gest_route , id_dept, id_commune FROM unique_hors_commune ;

--création des infrastructure
INSERT INTO emission.infra_route(id_route, declivite, nb_voie, largeur, sens_circulation, id_rvt_nature, id_rvt_etat, id_rvt_granulo, is_rvt_sec)
WITH 
complet AS (
SELECT id_route, 
       trunc(random() * (-6) + 3) declivite,
       trunc(random() * 5 + 1) nb_voie,
       trunc(random() * 25 + 1) largeur,
       trunc(random() * 16 + 1) sens_circulation,
       trunc(random() * 18 + 1) id_rvt_nature,
       trunc(random() * 4 + 1) id_rvt_etat,
       trunc(random() * 16 + 1) id_rvt_granulo,
       (round(random())::int)::boolean is_rvt_sec
 FROM emission.route
 OFFSET 10000),
partiel AS (
SELECT id_route, 
       NULL::int declivite,
       trunc(random() * 5 + 1) nb_voie,
       trunc(random() * 25 + 1) largeur,
       trunc(random() * 16 + 1) sens_circulation,
       NULL::int id_rvt_nature,
       NULL::int id_rvt_etat,
       NULL::int id_rvt_granulo,
       NULL::boolean is_rvt_sec
 FROM emission.route
 limit 10000),
vide AS (
SELECT id_route, 
       NULL::int declivite,
       NULL::int nb_voie,
       NULL::int largeur,
       NULL::int sens_circulation,
       NULL::int id_rvt_nature,
       NULL::int id_rvt_etat,
       NULL::int id_rvt_granulo,
       NULL::boolean is_rvt_sec
 FROM emission.route
 OFFSET 25000)
SELECT * FROM partiel UNION ALL SELECT * FROM complet UNION ALL SELECT * FROM vide ;
  
-- trafic routier
-- comme pour le ferroviaire, penser à faire varier : 
-- indicateur moyen journlier ou horaire
-- indicateur (débit, vitesse, PL)
-- nature de trafic
INSERT INTO emission.trafic_routier (id_infra_route, id_source_de_bruit, date_heure_debut, date_heure_fin, indicateur, valeur, trafic_nature)
WITH
infra AS (
SELECT array_agg(id_infra_route) id_infra_route
 FROM  (SELECT id_infra_route 
 		FROM emission.infra_route ivf 
 		ORDER BY random()
 		LIMIT (SELECT count(*) FROM emission.source_de_bruit WHERE type_source_bruit = 1)) t),
emission AS (
SELECT array_agg(sdb.id_source_de_bruit) id_source_de_bruit
 FROM emission.source_de_bruit sdb
 WHERE sdb.type_source_bruit = 1),
lien_infra_emission AS (
SELECT UNNEST(id_source_de_bruit) id_source_de_bruit, UNNEST(id_infra_route)  id_infra_route
 FROM infra, emission),
route_par_source_constat AS (
SELECT UNNEST(array_agg(sdb.id_source_de_bruit)) id_source_de_bruit, 
       UNNEST(array_agg(sdb.id_infra_route)) id_infra_route,
       UNNEST(array_agg(mb.date_debut)) date_debut, UNNEST(array_agg(mb.date_fin)) date_fin, 
       1 trafic_nature
 FROM lien_infra_emission sdb JOIN mesure_bruit.relation_source_mesure rsm using(id_source_de_bruit)
                                   JOIN mesure_bruit.mesure_bruit mb ON mb.id_mesure_bruit = rsm.id_mesure_bruit),
indicateur_moyen_journalier AS (
SELECT id_infra_route,
       id_source_de_bruit,
       date_debut date_heure_debut,
       NULL::timestamptz date_heure_fin,
       trafic_nature,
       generate_series(17, 19) indicateur
  FROM route_par_source_constat 
  LIMIT 20),
debits_horaires_constat AS (
-- gestion des débits horaires
 SELECT id_infra_route,
       id_source_de_bruit,
       generate_series(date_debut, (date_fin::timestamptz - '1 hour'::INTERVAL), '1 hour')::timestamptz date_heure_debut,   
       generate_series((date_debut::timestamptz + '1 hour'::INTERVAL), date_fin, '1 hour')::timestamptz date_heure_fin,
       trafic_nature,
       t.indicateur
  FROM route_par_source_constat CROSS JOIN (SELECT UNNEST(ARRAY[9,10, 16, 17]) indicateur) t
  OFFSET 20),
tout_indic AS (
SELECT * FROM debits_horaires_constat UNION ALL SELECT * FROM indicateur_moyen_journalier),
tout_indic_nettoye AS (
 SELECT DISTINCT id_infra_route, -- là je met le distcint pour aller vite ais x'est pas propre
       id_source_de_bruit,
       CASE WHEN indicateur IN (1,2,3,4,5,6,7,8,18,19) THEN NULL ELSE date_heure_debut END,
       CASE WHEN indicateur IN (1,2,3,4,5,6,7,8,18,19) THEN NULL ELSE date_heure_fin END,
       trafic_nature,
       indicateur
   FROM tout_indic),
constat_1 AS (
SELECT *
 FROM tout_indic_nettoye
 WHERE date_heure_debut IS NULL
 ORDER BY id_infra_route, id_source_de_bruit, date_heure_debut, indicateur
 LIMIT 10),
constat_2 AS (
SELECT * FROM constat_1 
UNION ALL SELECT * FROM tout_indic_nettoye WHERE date_heure_debut IS NOT null),
long_terme AS (
SELECT DISTINCT  id_infra_route, 
                 id_source_de_bruit, 
                 NULL::timestamptz date_heure_debut, 
                 NULL::timestamptz date_heure_fin, 
                 2 trafic_nature, 
                 s indicateur
 FROM constat_2, generate_series(18,19) s),
tout_avant_valeur AS (
SELECT * FROM constat_2 UNION ALL SELECT * FROM long_terme),
donnees_finale AS (
SELECT id_infra_route, 
       id_source_de_bruit, 
       date_heure_debut, 
       date_heure_fin,
       indicateur, 
       CASE WHEN indicateur IN (12, 13, 14, 15, 16, 17) THEN trunc(random()*130+30)
            WHEN indicateur IN (1, 3, 5, 7, 18) THEN trunc(random()*80000+500)
            WHEN indicateur IN (2, 4, 6, 8, 19) THEN round((random()*40+1)::numeric, 2)
            WHEN indicateur IN (9,10) THEN trunc(random()*1500+150)
            ELSE NULL END valeur,
       trafic_nature
 FROM tout_avant_valeur)
SELECT * FROM donnees_finale;


/* ===========================================================
 * 4. Description du sol
 =============================================================*/

-- seules qq mesures vont faire l'objet d'une description
INSERT INTO propagation.description_sol (id_source_mesure, id_nature_sol, pourcentage_occupation, id_hygro_sol, epaisseur_neige, etat_neige)
WITH 
base AS (
SELECT rsm.id_source_mesure
 FROM emission.source_de_bruit sdb JOIN mesure_bruit.relation_source_mesure rsm using(id_source_de_bruit)
                                   JOIN mesure_bruit.mesure_bruit mb ON mb.id_mesure_bruit = rsm.id_mesure_bruit
 WHERE sdb.type_source_bruit IN (1, 2)
 LIMIT 15),
affectation_sol AS (
SELECT id_source_mesure, 
       generate_series(trunc(random() * 3 + 2)::integer, trunc(random() * 4 + 4)::integer, trunc(random() * 4 + 1)::integer) id_nature_sol, 
       trunc(random() * 5 + 1)::integer id_hygro_sol
 FROM base),
ident_pb AS (
SELECT id_source_mesure, 
       id_nature_sol, 
       id_hygro_sol, 
       NULL::int epaisseur_neige, 
       NULL::int etat_neige,
       count(*) OVER (PARTITION BY id_source_mesure) id_temp
 FROM affectation_sol)
SELECT id_source_mesure, 
       id_nature_sol, 
       100 / id_temp pourcentage_occupation,
       id_hygro_sol, 
       CASE WHEN id_hygro_sol = 5 THEN trunc(random()*50+1)::int ELSE NULL::int END epaisseur_neige, 
       CASE WHEN id_hygro_sol = 5 THEN trunc(random()*3+1)::int ELSE NULL::int END etat_neige       
 FROM ident_pb ;
 

 
 
/* ===========================================================
 * 4. resultats de mesure
 =============================================================*/
 
 -- on s'attache surtout à décrire les indicateurs et à proposer unexemple de décomposition horaire d'un Leq
 -- il faut aussi qq exemple de resultat constat et long terme surdes routes
INSERT INTO mesure_bruit.resultat_mesure_bruit (id_mesure_bruit, id_nature_mesure, id_indicateur, id_unite_grandeur, 
id_ponderation, debut_periode, fin_periode, is_periode_reference, valeur, incertitude, commentaire)
WITH 
base as(
SELECT DISTINCT sdb.id_source_de_bruit, mb.id_mesure_bruit, sdb.type_source_bruit, sdb.id_source_de_bruit, tr.trafic_nature nature_trafic_route,
                mb.date_debut, mb.date_fin
 FROM emission.source_de_bruit sdb JOIN mesure_bruit.relation_source_mesure rsm using(id_source_de_bruit)
                                   JOIN mesure_bruit.mesure_bruit mb ON mb.id_mesure_bruit = rsm.id_mesure_bruit
                                   LEFT JOIN emission.trafic_routier tr ON tr.id_source_de_bruit = sdb.id_source_de_bruit),
multiplication_aleatoire_lignes as(
SELECT id_mesure_bruit, generate_series(0,2, trunc(random()*2+1)::int) i
 FROM mesure_bruit.mesure_bruit mb),
filtre_indic AS (
 SELECT *, count(*) OVER (PARTITION BY id_mesure_bruit) save
  FROM multiplication_aleatoire_lignes),
filtre AS (
SELECT * 
 FROM filtre_indic
 WHERE i > 0 OR (i=0 AND save = 1)),
base_indic AS (
 SELECT DISTINCT id_mesure_bruit, 
        trunc(random()*11+1)::int id_indicateur,
        trunc(random()*4+1)::int id_ponderation
 FROM filtre),
unite as(
 SELECT DISTINCT id_mesure_bruit,
        id_indicateur,
        CASE WHEN id_indicateur != 2 THEN 1 ELSE 4 END id_unite_grandeur,
        id_ponderation,
        mb.date_debut, 
        mb.date_fin,
        nature_trafic_route,
        (round(random())::int)::boolean is_periode_reference,
        CASE WHEN id_indicateur = 4 THEN round((random()*3+1)::NUMERIC, 1) ELSE  NULL::float END  incertitude,
        CASE WHEN id_indicateur = 5 THEN 'un comm quelconque' ELSE NULL::TEXT END commentaire
   FROM base_indic LEFT JOIN base mb USING(id_mesure_bruit)),
donnees_horaire_filtre AS (
SELECT *
 FROM unite
 WHERE id_indicateur = 1 AND nature_trafic_route IS null
 ORDER BY id_mesure_bruit, id_indicateur
 LIMIT 1),
donnees_horaire_hors_filtre as(
SELECT *
 FROM unite
 WHERE id_indicateur = 1 AND nature_trafic_route IS null
 ORDER BY id_mesure_bruit, id_indicateur
 OFFSET 1),
donnees_horaires AS (
SELECT id_mesure_bruit,
       1 id_nature_mesure, 
       id_indicateur, 
       id_unite_grandeur, 
       id_ponderation,
       generate_series(date_debut, (date_fin::timestamptz - '1 hour'::INTERVAL), '1 hour')::timestamptz debut_periode,   
       generate_series((date_debut::timestamptz + '1 hour'::INTERVAL), date_fin, '1 hour')::timestamptz fin_periode,
       FALSE is_periode_reference, 
       round((random()*100+40)::numeric, 1)::float valeur, 
       incertitude, 
       commentaire
 FROM donnees_horaire_filtre),
donnees_agreges AS (
SELECT *
 FROM unite
 WHERE id_indicateur > 1 OR  nature_trafic_route IS NOT NULL
UNION SELECT * FROM donnees_horaire_hors_filtre
 ORDER BY id_mesure_bruit, id_indicateur),
resultats as(
SELECT id_mesure_bruit,
       CASE WHEN nature_trafic_route IS NULL THEN 1 ELSE nature_trafic_route END id_nature_mesure, 
       id_indicateur, 
       id_unite_grandeur, 
       id_ponderation,
       NULL::timestamptz debut_periode,
       NULL::timestamptz fin_periode,
       TRUE is_periode_reference, 
       CASE WHEN id_indicateur = 2 THEN trunc(random()*5+1)::float 
            ELSE round((random()*100+40)::numeric, 1)::float END valeur, 
       incertitude, 
       commentaire FROM donnees_agreges)
SELECT * FROM resultats UNION SELECT * FROM donnees_horaires ORDER BY id_mesure_bruit;
  
--frequence
INSERT INTO mesure_bruit.bruit_frequence (id_resultat_mesure_bruit, id_nature_bande_freq, frequence_centrale, niveau)
SELECT 
(SELECT id_resultat_mesure_bruit FROM mesure_bruit.resultat_mesure_bruit
 WHERE id_nature_mesure = 1 AND id_indicateur = 1
 LIMIT 1) id_resultat_mesure_bruit,
  1 id_nature_bande_freq,
 unnest(ARRAY[31, 63, 125, 250, 500, 1000, 2000, 4000, 8000, 16000]) frequence_centrale,
 round((random()*80+40)::NUMERIC, 2) niveau
UNION 
SELECT 
(SELECT id_resultat_mesure_bruit FROM mesure_bruit.resultat_mesure_bruit
 WHERE id_nature_mesure = 1 AND id_indicateur = 1
 ORDER BY random()
 LIMIT 1) id_resultat_mesure_bruit,
  2 id_nature_bande_freq,
 unnest((SELECT array_agg(frequence_centrale) FROM mesure_bruit.enum_bande_freq)) frequence_centrale,
 round((random()*80+40)::NUMERIC, 2) niveau ;

 
 