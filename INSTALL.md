# Installation Guide
En cas de problème, vous pouvez contacter martin.schoreisz@cerema.fr

## Data Collection

Il n'y a pas de besoin de collecter la donnée. 

Par contre l'installation d'une base postgres / postgis est nécéssaire. Cf le dossier [modele de données](https://gitlab.com/data-challenge-gd4h/bambin/-/tree/main/modele_donnees) pour plus de détails.
 2 possibilités existent : créer sa propre base ou utiliser une base exemple fournie dans le projet Git


## Dependencies


- docker pour executer et docker desktop pour faire fonctionner facilement sur windows  le back-office de l'application
- nodejs 
- postgres 13 ou supérieur 
- extension postgis 

## Installation

Le prototype fonctionne ensuivant ces étapes : 
Etapes préalables optionnelles selon votre configuration de départ 
01.a Installer PostgreSQL   https://www.postgresql.org/download/ 
01.b Installer postgis https://postgis.net/documentation/getting_started/install_windows/released_versions/

02. Dans votre serveur postgres, isolez une base de données "bambin" vide dont vous renseignerez les chemins d'accès à l'étape 06


03. Installer Docker /  Docker Desktop / la solution a été testée avec Ubuntu sous docker desktop
04. installer nodejs (9.5 au moment du projet)


05. télécharger le présent repo 
06. ajouter à la racine du repo un fichier .env avec à l’interieur : avec la configuration de votre base de données cible 
    
    ```
        POSTGRES_USER: postgres
        POSTGRES_PASSWORD: 123
        POSTGRES_DB: bambin
        POSTGRES_HOST: localhost
    ```
   
06.b vous pouvez charger la base de données de demo depuis le script Creation_MPD.sql du dossier modele_donnees   
07.a se placer dans le dossier backend
07.b. lancer docker puis en se plaçant dans le dossier contenant le fichier `docker-compose.yml` et en lançant la commande `docker compose up -d --build app`
 
08. se placer dans le dossier frontend 
08.b et lancer les commande `npm install` puis `npm run dev`
09. lancer http://localhost:8080/ dans le navigateur pour accéder à l’interface front
09.b Exemple du resultat https://gitlab.com/data-challenge-gd4h/bambin/-/blob/main/frontend/demo_screenshot/demo1.png

10. lancer http://localhost:8000/docs#/ dans le navigateur pour accéder à FastAPI, qui permet de tester les connections.

11. une fois les données complétées, elles sont envoyées à la base sur le serveur OVH, cf les identifiants au-dessus (étape 06), avec le port classique de postgres.
