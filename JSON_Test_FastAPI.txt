# *****************************************************************************************
# Exemple incluant le remplissage des tables liées au trafic routier
# type_source_bruit=1 obligatoirement
# *****************************************************************************************
{
  "date_debut": "2023-05-25T08:30:01.734Z",
  "date_fin": "2023-05-25T09:30:01.734Z",
  "hauteur_mesure": 1,
  "maitre_ouvrage": "string",
  "is_ponctuel": true,
  "is_point_fixe": true,
  "is_exterieur": true,
  "id_materiel_mesure": 1,
  "duree_integration": 1,
  "x_coordinate_epsg2154": 300000,
  "y_coordinate_epsg2154": 7000000,
  "comment_localisation": "string",
  "resultats": 
	[
		{
		  "debut_periode": "2023-05-29T20:57:05.537Z",
		  "fin_periode": "2023-05-29T21:57:05.537Z",
		  "is_periode_reference": true,
		  "valeur": 50
		},
		{
		  "id_indicateur": 8,
		  "debut_periode": "2023-05-29T20:57:05.537Z",
		  "fin_periode": "2023-05-29T21:57:05.537Z",
		  "valeur": 80
		}
	],
  "type_source_bruit": 1,
  "id_nom_route": 4,
  "id_gest_route": 1,
  "id_dept": 7,
  "trafics_routiers":
  [
    {
      "date_heure_debut": "2023-05-29T20:57:05.537Z",
      "date_heure_fin": "2023-05-29T21:00:05.537Z",
      "valeur": 12.5
    },
    {
      "date_heure_debut": "2023-05-29T21:15:05.537Z",
      "date_heure_fin": "2023-05-29T21:57:05.537Z",
      "valeur": 15
    }
  ]
}

# *****************************************************************************************
# Exemple pour remplir les table mesure_bruit, resultat_mesure_bruit et localisation_mesure
# *****************************************************************************************
{
  "date_debut": "2023-05-25T08:30:01.734Z",
  "date_fin": "2023-05-25T09:30:01.734Z",
  "hauteur_mesure": 1,
  "maitre_ouvrage": "string",
  "is_ponctuel": true,
  "is_point_fixe": true,
  "is_exterieur": true,
  "id_materiel_mesure": 1,
  "duree_integration": 1,
  "x_coordinate_epsg2154": 300000,
  "y_coordinate_epsg2154": 7000000,
  "comment_localisation": "string",
  "resultats": 
	[
		{
		  "debut_periode": "2023-05-29T20:57:05.537Z",
		  "fin_periode": "2023-05-29T21:57:05.537Z",
		  "is_periode_reference": true,
		  "valeur": 50
		},
		{
		  "id_indicateur": 8,
		  "debut_periode": "2023-05-29T20:57:05.537Z",
		  "fin_periode": "2023-05-29T21:57:05.537Z",
		  "valeur": 80
		}
	]
}

# ********************************************************
# Exemple minimal pour remplir la table mesure_bruit
# ********************************************************
{
  "date_debut": "2023-05-25T08:30:01.734Z",
  "date_fin": "2023-05-25T09:30:01.734Z",
  "hauteur_mesure": 1,
  "maitre_ouvrage": "string",
  "is_ponctuel": true,
  "is_point_fixe": true,
  "is_exterieur": true,
  "id_materiel_mesure": 1,
  "duree_integration": 1,
  "x_coordinate_epsg2154": 300000,
  "y_coordinate_epsg2154": 7000000,
  "comment_localisation": "string",
}