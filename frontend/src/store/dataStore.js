import { defineStore} from "pinia";

export const useDataStore = defineStore('data', {
    state: () => ({
        communes : [
            {
                "id_commune" : 13106,
                "typecom" : "COM",
                "com" : "33003",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "1",
                "ncc" : "AMBARES ET LAGRAVE",
                "nccenr" : "Ambarès-et-Lagrave",
                "commune" : "Ambarès-et-Lagrave",
                "can" : "3326",
                "comparent" : null
            },
            {
                "id_commune" : 13107,
                "typecom" : "COM",
                "com" : "33004",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "1",
                "ncc" : "AMBES",
                "nccenr" : "Ambès",
                "commune" : "Ambès",
                "can" : "3326",
                "comparent" : null
            },
            {
                "id_commune" : 13116,
                "typecom" : "COM",
                "com" : "33013",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "1",
                "ncc" : "ARTIGUES PRES BORDEAUX",
                "nccenr" : "Artigues-près-Bordeaux",
                "commune" : "Artigues-près-Bordeaux",
                "can" : "3317",
                "comparent" : null
            },
            {
                "id_commune" : 13136,
                "typecom" : "COM",
                "com" : "33032",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "BASSENS",
                "nccenr" : "Bassens",
                "commune" : "Bassens",
                "can" : "3317",
                "comparent" : null
            },
            {
                "id_commune" : 13143,
                "typecom" : "COM",
                "com" : "33039",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "BEGLES",
                "nccenr" : "Bègles",
                "commune" : "Bègles",
                "can" : "3396",
                "comparent" : null
            },
            {
                "id_commune" : 13159,
                "typecom" : "COM",
                "com" : "33056",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "BLANQUEFORT",
                "nccenr" : "Blanquefort",
                "commune" : "Blanquefort",
                "can" : "3325",
                "comparent" : null
            },
            {
                "id_commune" : 13166,
                "typecom" : "COM",
                "com" : "33063",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "BORDEAUX",
                "nccenr" : "Bordeaux",
                "commune" : "Bordeaux",
                "can" : "3397",
                "comparent" : null
            },
            {
                "id_commune" : 13168,
                "typecom" : "COM",
                "com" : "33065",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "BOULIAC",
                "nccenr" : "Bouliac",
                "commune" : "Bouliac",
                "can" : "3309",
                "comparent" : null
            },
            {
                "id_commune" : 13172,
                "typecom" : "COM",
                "com" : "33069",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "2",
                "ncc" : "BOUSCAT",
                "nccenr" : "Bouscat",
                "commune" : "Le Bouscat",
                "can" : "3307",
                "comparent" : null
            },
            {
                "id_commune" : 13178,
                "typecom" : "COM",
                "com" : "33075",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "BRUGES",
                "nccenr" : "Bruges",
                "commune" : "Bruges",
                "can" : "3307",
                "comparent" : null
            },
            {
                "id_commune" : 13197,
                "typecom" : "COM",
                "com" : "33096",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "CARBON BLANC",
                "nccenr" : "Carbon-Blanc",
                "commune" : "Carbon-Blanc",
                "can" : "3326",
                "comparent" : null
            },
            {
                "id_commune" : 13220,
                "typecom" : "COM",
                "com" : "33119",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "CENON",
                "nccenr" : "Cenon",
                "commune" : "Cenon",
                "can" : "3309",
                "comparent" : null
            },
            {
                "id_commune" : 13263,
                "typecom" : "COM",
                "com" : "33162",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "1",
                "ncc" : "EYSINES",
                "nccenr" : "Eysines",
                "commune" : "Eysines",
                "can" : "3325",
                "comparent" : null
            },
            {
                "id_commune" : 13268,
                "typecom" : "COM",
                "com" : "33167",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "FLOIRAC",
                "nccenr" : "Floirac",
                "commune" : "Floirac",
                "can" : "3309",
                "comparent" : null
            },
            {
                "id_commune" : 13293,
                "typecom" : "COM",
                "com" : "33192",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "GRADIGNAN",
                "nccenr" : "Gradignan",
                "commune" : "Gradignan",
                "can" : "3324",
                "comparent" : null
            },
            {
                "id_commune" : 13301,
                "typecom" : "COM",
                "com" : "33200",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "2",
                "ncc" : "HAILLAN",
                "nccenr" : "Haillan",
                "commune" : "Le Haillan",
                "can" : "3318",
                "comparent" : null
            },
            {
                "id_commune" : 13349,
                "typecom" : "COM",
                "com" : "33249",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "LORMONT",
                "nccenr" : "Lormont",
                "commune" : "Lormont",
                "can" : "3317",
                "comparent" : null
            },
            {
                "id_commune" : 13372,
                "typecom" : "COM",
                "com" : "33273",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "MARTIGNAS SUR JALLE",
                "nccenr" : "Martignas-sur-Jalle",
                "commune" : "Martignas-sur-Jalle",
                "can" : "3319",
                "comparent" : null
            },
            {
                "id_commune" : 13380,
                "typecom" : "COM",
                "com" : "33281",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "MERIGNAC",
                "nccenr" : "Mérignac",
                "commune" : "Mérignac",
                "can" : "3398",
                "comparent" : null
            },
            {
                "id_commune" : 13410,
                "typecom" : "COM",
                "com" : "33312",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "PAREMPUYRE",
                "nccenr" : "Parempuyre",
                "commune" : "Parempuyre",
                "can" : "3325",
                "comparent" : null
            },
            {
                "id_commune" : 13415,
                "typecom" : "COM",
                "com" : "33318",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "PESSAC",
                "nccenr" : "Pessac",
                "commune" : "Pessac",
                "can" : "3399",
                "comparent" : null
            },
            {
                "id_commune" : 13470,
                "typecom" : "COM",
                "com" : "33376",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "SAINT AUBIN DE MEDOC",
                "nccenr" : "Saint-Aubin-de-Médoc",
                "commune" : "Saint-Aubin-de-Médoc",
                "can" : "3328",
                "comparent" : null
            },
            {
                "id_commune" : 13527,
                "typecom" : "COM",
                "com" : "33434",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "SAINT LOUIS DE MONTFERRAND",
                "nccenr" : "Saint-Louis-de-Montferrand",
                "commune" : "Saint-Louis-de-Montferrand",
                "can" : "3326",
                "comparent" : null
            },
            {
                "id_commune" : 13542,
                "typecom" : "COM",
                "com" : "33449",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "SAINT MEDARD EN JALLES",
                "nccenr" : "Saint-Médard-en-Jalles",
                "commune" : "Saint-Médard-en-Jalles",
                "can" : "3328",
                "comparent" : null
            },
            {
                "id_commune" : 13578,
                "typecom" : "COM",
                "com" : "33487",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "SAINT VINCENT DE PAUL",
                "nccenr" : "Saint-Vincent-de-Paul",
                "commune" : "Saint-Vincent-de-Paul",
                "can" : "3326",
                "comparent" : null
            },
            {
                "id_commune" : 13609,
                "typecom" : "COM",
                "com" : "33519",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "2",
                "ncc" : "TAILLAN MEDOC",
                "nccenr" : "Taillan-Médoc",
                "commune" : "Le Taillan-Médoc",
                "can" : "3328",
                "comparent" : null
            },
            {
                "id_commune" : 13612,
                "typecom" : "COM",
                "com" : "33522",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "TALENCE",
                "nccenr" : "Talence",
                "commune" : "Talence",
                "can" : "3331",
                "comparent" : null
            },
            {
                "id_commune" : 13640,
                "typecom" : "COM",
                "com" : "33550",
                "reg" : "75",
                "dep" : "33",
                "ctcd" : "33D",
                "arr" : "332",
                "tncc" : "0",
                "ncc" : "VILLENAVE D ORNON",
                "nccenr" : "Villenave-d'Ornon",
                "commune" : "Villenave-d'Ornon",
                "can" : "3333",
                "comparent" : null
            }
        ], 
        trafics : [
            {
                "id_indicateur_trafic_routier" : 1,
                "indicateur_trafic_routier" : "TMJA"
            },
            {
                "id_indicateur_trafic_routier" : 2,
                "indicateur_trafic_routier" : "pourcentage de PL annuel"
            },
            {
                "id_indicateur_trafic_routier" : 9,
                "indicateur_trafic_routier" : "débit VL"
            },
            {
                "id_indicateur_trafic_routier" : 10,
                "indicateur_trafic_routier" : "débit PL"
            },
            {
                "id_indicateur_trafic_routier" : 12,
                "indicateur_trafic_routier" : "vitesse moyenne VL"
            },
            {
                "id_indicateur_trafic_routier" : 13,
                "indicateur_trafic_routier" : "vitesse moyenne PL"
            },
            {
                "id_indicateur_trafic_routier" : 18,
                "indicateur_trafic_routier" : "TMJ"
            },
            {
                "id_indicateur_trafic_routier" : 19,
                "indicateur_trafic_routier" : "pourcentage de PL"
            }
        ], departments : [
            {
              "id_dept" : 34,
              "dept" : "33",
              "reg" : "75",
              "cheflieu" : "33063",
              "tncc" : "3",
              "ncc" : "GIRONDE",
             "nccenr" : "Gironde",
             "libelle" : "Gironde"
              }
          ],
          administrators : [
                {
                    "id_gest_route" : 73,
                    "gest_route" : "ASF"
                },
                {
                    "id_gest_route" : 79,
                    "gest_route" : "DIR Atlantique"
                }
            ],
            roadNames : [
                {
                    "id_nom_route" : 20112,
                    "nom_route" : "N230"
                },
                {
                    "id_nom_route" : 20594,
                    "nom_route" : "A62"
                },
                {
                    "id_nom_route" : 21249,
                    "nom_route" : "A63"
                },
                {
                    "id_nom_route" : 21688,
                    "nom_route" : "A630"
                },
                {
                    "id_nom_route" : 22910,
                    "nom_route" : "N89"
                },
                {
                    "id_nom_route" : 24541,
                    "nom_route" : "N10"
                },
                {
                    "id_nom_route" : 27985,
                    "nom_route" : "A10"
                }
            ],

    })
})