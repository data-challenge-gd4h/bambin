import { defineStore } from 'pinia';

export const useFormStore = defineStore('form', {
    state: () => ({
        step : 'home',
        formComplete : {
            maitre_ouvrage: '',
            maitre_oeuvre: '',
            is_point_fixe: true,
            is_exterieur: true,
            is_facade: true,
            is_champ: false,
            hauteur_mesure: 0,
            is_ponctuel: true,
            date_debut: '2023-06-02T15:04:25.421Z',
            date_fin: '2023-06-02T15:04:25.421Z',
            duree_integration: 1,
            id_materiel_mesure: 0,
            comment_mesure: '',
            x_coordinate_epsg2154: 0,
            y_coordinate_epsg2154: 0,
            comment_localisation: '',
            resultats: [],
            type_source_bruit : 1,
            id_nom_route : 1,
            id_gest_route : 1,
            id_dept : 1,
            trafics_routiers : [],
        }
    }),
    actions : {
        changeStepValue(value) {
            return this.step = value;
        },
        addMaterielMesure(data) {
            if(this.formMateriel) {
                return console.log('Matériel déjà fournie');
            }
            else {
                console.log(data)
                this.formComplete.id_materiel_mesure = data;
                return console.log(this.formComplete);
            }
        },
        addGeo(data) {
            if(this.formGeo) {
                return console.log('coordonnée déjà fournie')
            }
            else {
                const { coordinateX, coordinateY} = data;
                this.formComplete.x_coordinate_epsg2154 = coordinateX;
                this.formComplete.y_coordinate_epsg2154 = coordinateY;
                return  console.log(this.formComplete);
                
            }
        },
        addMesure(data) {
            if(!data) {
                return console.log("Le formulaire est vide !");
            }
            else {
                                
                const {
                    fixedMesure, outsideMesure, heightMesure,
                    ponctuelleMesure, integrationDuration, comment, date
                } = data;
                const { sirenOuvrage, sirenOeuvre } = data.sirenNumber;
                this.formComplete.is_exterieur = outsideMesure;
                this.formComplete.duree_integration = integrationDuration;
                this.formComplete.comment_mesure = comment;
                this.formComplete.hauteur_mesure = heightMesure;
                this.formComplete.is_point_fixe = fixedMesure;
                this.formComplete.is_ponctuel = ponctuelleMesure;
                this.formComplete.maitre_oeuvre = parseInt(sirenOeuvre);
                this.formComplete.maitre_ouvrage = parseInt(sirenOuvrage);
                this.formComplete.date_debut = date[0];
                this.formComplete.date_fin = date[1];
            }
        },
        addResult(data) {
            this.formComplete.resultats.push(data);
            console.log(this.formComplete.resultats);
        },
        addRoad(data) {
            if(!data) {
                return console.log("Le formulaire est vide !")
            } else {
                const { roadId, roadAdministratorId, departmentId, communeId } = data;
                this.formComplete.id_dept = departmentId;
                this.formComplete.id_gest_route = roadAdministratorId;
                this.formComplete.id_nom_route = roadId;
                this.formComplete.id_commune = communeId;
                console.log(this.formComplete)
            }


        },
        addTrafic(data) {
            this.formComplete.trafics_routiers.push(data);
            console.log(this.formComplete.trafics_routiers);
        },
        handleChampFacadeValue(value) {
            if(value === true) {
                this.formComplete.is_champ = false;
                this.formComplete.is_facade = true;
            } else {
                this.formComplete.is_champ = true;
                this.formComplete.is_facade = false;
            }
        }
        
    },
});

