import { createRouter, createWebHistory } from 'vue-router'
import Home from "./views/Home.vue";
import RegisterForm from "./views/RegisterForm.vue";
import GlobalForm from './views/GlobalForm.vue';

export default createRouter({
    history : createWebHistory(),
    routes : [
        {
            path : '/home',
            component : Home,
        },
        {
            path : '/registerForm',
            component : RegisterForm
        },
        {
            path : '/',
            component : GlobalForm
        },
    ]
})
