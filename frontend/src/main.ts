import { createApp } from "vue";
import "./style.css";
import { plugin, defaultConfig } from "@formkit/vue";
import App from "./App.vue";
import { createPinia } from "pinia";
import axios from "axios";
import router from './vue-router';

const pinia = createPinia();

axios.defaults.withCredentials = true;
axios.defaults.baseURL = "http://localhost:8000/";


createApp(App).use(plugin, defaultConfig).use(router).use(pinia).mount("#app");
